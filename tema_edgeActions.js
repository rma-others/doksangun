/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "${_Turuncu_bant}", "mousedown", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         $("#Stage").css("margin","auto")

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_Kampanya_ButtonCopy}", "mouseover", function(sym, e) {
         
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on1").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_Kampanya_ButtonCopy}", "mouseout", function(sym, e) {
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on1").hide();
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_kampanya_hakkinda}", "mouseover", function(sym, e) {
         
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on1").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_kampanya_hakkinda}", "mouseout", function(sym, e) {
         
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on1").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sloganimiz}", "mouseover", function(sym, e) {
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on2Copy").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sloganimiz}", "mouseout", function(sym, e) {
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on2Copy").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sloganimizCopy}", "mouseover", function(sym, e) {
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on2Copy2").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sloganimizCopy}", "mouseout", function(sym, e) {
         // insert code to be run when the mouse is moved off the object
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("Mouse_on2Copy2").hide();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         

      });
      //Edge binding end

      

      

      

      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_menuG}", "mouseover", function(sym, e) {
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("menuG").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_menuG}", "mouseout", function(sym, e) {
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("menuG").hide();
         

      });
      //Edge binding end

      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_icerigi}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         // insert code for mouse click here
         // play the timeline from the given position (ms or label)
         sym.play(17750);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_icerigiCopy}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         // insert code for mouse click here
         // play the timeline from the given position (ms or label)
          sym.play(17750);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_icerigi}", "mouseover", function(sym, e) {
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("icerigiCopy").show();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_icerigiCopy}", "mouseout", function(sym, e) {
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("icerigiCopy").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_kampanya_hakkinda}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         

      });
      //Edge binding end

      

      

      

      

      

      Symbol.bindElementAction(compId, symbolName, "${_amaci}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         // play the timeline from the given position (ms or label)
         sym.play(21500);
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_amaciCopy}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("index.html", "_self");
         // play the timeline from the given position (ms or label)
         sym.play(21500);
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_amaciCopy}", "mouseout", function(sym, e) {
         // Hide an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("amaciCopy").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_amaci}", "mouseover", function(sym, e) {
         // Show an Element.
         //  (sym.$("name") resolves an Edge Animate element name to a DOM
         //  element that can be used with jQuery)
         sym.$("amaciCopy").show();
         

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_sloganimiz}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("tema.html", "_self");
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 127601, function(sym, e) {
         // play the timeline from the given position (ms or label)
         sym.play(6000);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sloganimizCopy}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("bizeulasin.html", "_self");
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'Symbol_1'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_you_tube_butt}", "mouseover", function(sym, e) {
         sym.play();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_you_tube_butt}", "mouseout", function(sym, e) {
         sym.playReverse();
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_you_tube_butt}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("http://www.90gun.org/video1.html", "_self");
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
      });
      //Edge binding end

   })("Symbol_1");
   //Edge symbol end:'Symbol_1'

   //=========================================================
   
   //Edge symbol: 'tamambutton'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_RoundRect2}", "click", function(sym, e) {
         // Navigate to a new URL in the current window
         // (replace "_self" with appropriate target attribute for a new window)
         window.open("http://www.90gun.org/iletisim.htm", "_blank");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_RoundRect2}", "mouseover", function(sym, e) {
         sym.play();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_RoundRect2}", "mouseout", function(sym, e) {
         sym.playReverse();
         

      });
      //Edge binding end

   })("tamambutton");
   //Edge symbol end:'tamambutton'

   //=========================================================
   
   //Edge symbol: 'ic_button'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_sol_ic}", "mouseover", function(sym, e) {
         sym.play();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_sol_ic}", "mouseout", function(sym, e) {
         sym.playReverse();
         

      });
      //Edge binding end

   })("ic_button");
   //Edge symbol end:'ic_button'

})(jQuery, AdobeEdge, "EDGE-91745653");