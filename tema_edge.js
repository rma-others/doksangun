/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'RoundRect2',
            type:'rect',
            rect:['0px','502px','983px','177px','auto','auto'],
            borderRadius:["15px 15px","15px 15px","15px 15px","15px 15px"],
            fill:["rgba(0,174,239,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         },
         {
            id:'Kampanya_ButtonCopy',
            type:'rect',
            rect:['306px','452px','136px','35px','auto','auto'],
            cursor:['context-menu'],
            borderRadius:["0px","0px","0px","9px 9px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy2',
            type:'rect',
            rect:['442px','452px','124px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","0px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy3',
            type:'rect',
            rect:['566px','452px','113px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","9px 9px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'seperation_',
            type:'image',
            rect:['441px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'seperation_Copy',
            type:'image',
            rect:['565px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'Mouse_on1',
            display:'none',
            type:'image',
            rect:['311px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy',
            display:'none',
            type:'image',
            rect:['437px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy2',
            display:'none',
            type:'image',
            rect:['550px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'kampanya_hakkinda',
            type:'text',
            rect:['306px','462px','140px','17px','auto','auto'],
            cursor:['pointer'],
            text:"Çalışma hakkında",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimiz',
            type:'text',
            rect:['448px','462px','106px','17px','auto','auto'],
            cursor:['pointer'],
            text:"90 Gün Teması",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimizCopy',
            type:'text',
            rect:['567px','462px','112px','17px','auto','auto'],
            cursor:['pointer'],
            text:"Bize ulaşın",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'Text3',
            type:'text',
            rect:['877','768','auto','auto','auto','auto'],
            text:"©Copyright 2013 Türkiye Kamu Hastaneleri Kurumu",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',15,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'RoundRect',
            type:'rect',
            rect:['1px','701px','977px','56px','auto','auto'],
            fill:["rgba(0,174,239,1)"],
            stroke:[1,"rgb(150, 150, 150)","none"]
         },
         {
            id:'Text5',
            type:'text',
            rect:['57','710','auto','auto','auto','auto'],
            cursor:['wait'],
            text:"90 Gün Çalışması'nın tamamlanmasına 80 Gün kaldı.",
            align:"center",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',14,"rgba(255,255,255,1)","500","none","normal"]
         },
         {
            id:'puzzle_Sb',
            type:'image',
            rect:['0px','25','983px','427px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"puzzle_Sb.jpg",'0px','0px']
         },
         {
            id:'Puzzle_A-01',
            type:'image',
            rect:['0','25','77px','62px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_A-01.png",'0px','0px']
         },
         {
            id:'Puzzle_A-02',
            type:'image',
            rect:['55px','25','97px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_A-02.png",'0px','0px']
         },
         {
            id:'Puzzle_C-05',
            type:'image',
            rect:['282px','130px','97px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_C-05.png",'0px','0px']
         },
         {
            id:'Puzzle_F-12',
            type:'image',
            rect:['830px','330px','98px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_F-12.png",'0px','0px']
         },
         {
            id:'Puzzle_E-09',
            type:'image',
            rect:['394','351px','128px','104px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_E-09.png",'0px','0px']
         },
         {
            id:'Puzzle_B-07',
            type:'image',
            rect:['395','374','97px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_B-07.png",'0px','0px']
         },
         {
            id:'Puzzle_B-11',
            type:'image',
            rect:['375px','374px','97px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_B-11.png",'0px','0px']
         },
         {
            id:'Puzzle_E-02',
            type:'image',
            rect:['604px','369px','96px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_E-02.png",'0px','0px']
         },
         {
            id:'Puzzle_D-06',
            type:'image',
            rect:['732px','374px','98px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_D-06.png",'0px','0px']
         },
         {
            id:'Puzzle_C-09',
            type:'image',
            rect:['169','380','96px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_C-09.png",'0px','0px']
         },
         {
            id:'Puzzle_F-04',
            type:'image',
            rect:['717px','377px','98px','78px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_F-04.png",'0px','0px']
         },
         {
            id:'Puzzle_E-11',
            type:'image',
            rect:['152','376px','96px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_E-11.png",'0px','0px']
         },
         {
            id:'Puzzle_A-072',
            type:'image',
            rect:['429px','390px','96px','62px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_A-07.png",'0px','0px']
         },
         {
            id:'Puzzle_C-13',
            type:'image',
            rect:['453','373px','96px','79px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Puzzle_C-13.png",'0px','0px']
         },
         {
            id:'Text-leylek',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Güneye göç etmek amacıyla Türkiye’den uçmaya başlayan bir leylek, 90 gün içerisinde Afrika’ya ulaşıp, geri dönebilir.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelen',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Kararlı bir yaklaşımla ve hizmete inanmış kişilerle, 90 günde bir gökdelen inşa etmek mümkündür.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"90 günde Türkiye’nin en batı noktasından, en doğu noktasına yürüyerek ulaşmak mümkündür.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy2',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Dünya Sağlık Örgütü 90 günden uzun süren sağlık problemlerini ‘kronik hastalık’ olarak değerlendirmektedir.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy3',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Hiçbir şey, zamanı gelmiş bir düşünce kadar güçlü olamaz.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy4',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Vazgeçmek, geçici bir sorun için kalıcı bir çözüm olur.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy5',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Büyük işler başaran insanlar her zaman, hayallerini gerçekleştirebilmek için inatla ve dirençle başarısızlıklarının karşısında direnen kişiler olmuştur.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy7',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Hedefler, başarıya giden yoldaki ölçülebilir kilometre taşlarıdır.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy8',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Planlamasını önceden yapmayan bir insan,<br>hiçbir zaman öne geçemez.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy9',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Pusulanın beşinci yönünü: Şu anda nerede olduğunuzu kestiremiyorsanız, harita ve pusula hiçbir işe yaramaz.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy10',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Amacımızı belirleyebilir ve yaşamımızın her devresi için hedefler saptayabiliriz, ancak daha sonra harekete geçmezsek, hiçbir şeyi başaramayız.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy12',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"İlerlememizi periyodik olarak gözden geçirmek, harekete geçmek kadar önemlidir.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy13',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Başarıya ulaşmak için gerçekte ne kadar az zamanımız olduğunu fark etmeliyiz.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy14',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"İşlerimizi önem sırasına göre yapmaya genellikle direniriz, yine de zamanımızı bunun kadar iyi yöneten tek bir yöntem yoktur.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy18',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Organizasyon eksik olduğunda günümüz, amaçsızca ve başarısız şekilde boşa gidecektir.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy19',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Tarihteki tüm büyük adamların tek ortak paydası, yapmakta olduklarına inanmış olmalarıdır.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy20',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Başarılı insanlar, zamanın değerinin farkındadır.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy21',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Bir insanın başarısı ile başarısızlığı arasındaki ayırıcı çizgi, zamanını ne kadar iyi yönettiği konusunda  ortaya çıkar.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy22',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Stres bazılarının kırılmasına, <br>bazılarının da rekor kırmasına neden olur.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy23',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Hedef saptamazsak,<br>önceliklerimizi belirleyemeyiz.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy24',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Hedeflerimize ulaşmakla elde ettiğimiz, onlara ulaşma yolunda yaşadığımız değişimin yanında önemsiz kalır.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy25',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Hedefler, başarımızı harekete değil,<br>sonuçlara bakarak değerlendirmemizi sağlar.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy26',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Başarılı insanı belirleyen ilk özellik, tutumudur. Kişi olumlu tutum ve düşüncelere sahipse, zorluklarla uğraşmayı seviyor ve onların üstesinden gelmeyi hedefliyor ise başarılarının yarısını gerçekleştirmiş sayılır.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy28',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"Pratik insan, istediğini elde etmeyi bilendir. Filozof, insanların ne istemesi gerektiğini bilendir. Başarılı insan, istemesi gerekeni elde etmeyi bilendir.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'Text-gokdelenCopy29',
            type:'text',
            rect:['39px','554px','925px','117px','auto','auto'],
            text:"İçsel kaynaklarımızdan maksimum fayda sağlamanın anahtarı, kapsamlı düşünmek ve iyi hazırlanmış planlarla başarıyı önceden tasarlamaktır. ",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',32,"rgba(255,255,255,1)","600","none","normal"]
         },
         {
            id:'menuG',
            display:'none',
            type:'group',
            rect:['318','487','129','49','auto','auto'],
            c:[
            {
               id:'menu',
               type:'rect',
               rect:['6px','0px','124px','50px','auto','auto'],
               fill:["rgba(192,192,192,1)"],
               stroke:[0,"rgba(0,0,0,1)","none"]
            },
            {
               id:'Rectangle2',
               type:'rect',
               rect:['0px','23px','123px','3px','auto','auto'],
               fill:["rgba(0,174,239,0.9063)"],
               stroke:[0,"rgb(0, 0, 0)","none"]
            },
            {
               id:'amaci',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'amaciCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigi',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigiCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            }]
         },
         {
            id:'Turuncu_bant',
            type:'image',
            rect:['0px','0px','983px','25px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Turuncu_bant.png",'0px','0px'],
            boxShadow:["",0,3,4,-2,"rgba(0,0,0,0.65)"]
         }],
         symbolInstances: [

         ]
      },
   states: {
      "Base State": {
         "${_Text-gokdelenCopy12}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy5}": [
            ["style", "top", '535px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_icerigi}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '2px'],
            ["style", "font-size", '13px'],
            ["style", "top", '6px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '124px']
         ],
         "${_Puzzle_E-11}": [
            ["style", "top", '339px'],
            ["style", "height", '116px'],
            ["style", "opacity", '0'],
            ["style", "left", '152px'],
            ["style", "width", '141px']
         ],
         "${_Text-gokdelen}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_sloganimiz}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '444px'],
            ["style", "font-size", '13px'],
            ["style", "top", '461px'],
            ["style", "width", '121px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Text-gokdelenCopy7}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_C-09}": [
            ["style", "top", '361px'],
            ["style", "height", '98px'],
            ["style", "opacity", '0'],
            ["style", "left", '169px'],
            ["style", "width", '119px']
         ],
         "${_Text-gokdelenCopy18}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy4}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_D-06}": [
            ["style", "top", '350px'],
            ["style", "height", '102px'],
            ["style", "opacity", '0'],
            ["style", "left", '702px'],
            ["style", "width", '128px']
         ],
         "${_Kampanya_ButtonCopy2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "height", '35px'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '442px'],
            ["style", "width", '124px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '779px'],
            ["style", "width", '983px']
         ],
         "${_RoundRect2}": [
            ["style", "top", '502px'],
            ["style", "border-top-left-radius", [15,15], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [15,15], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '177px'],
            ["style", "border-top-right-radius", [15,15], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-left-radius", [15,15], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_Text-gokdelenCopy9}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_kampanya_hakkinda}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '311px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "width", '128px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Text-gokdelenCopy26}": [
            ["style", "top", '517px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy20}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy8}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy28}": [
            ["style", "top", '535px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_amaciCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "left", '1px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "cursor", 'pointer'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "display", 'none'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '124px']
         ],
         "${_Puzzle_F-04}": [
            ["style", "top", '338px'],
            ["style", "height", '117px'],
            ["style", "opacity", '0'],
            ["style", "left", '717px'],
            ["style", "width", '147px']
         ],
         "${_Text-gokdelenCopy29}": [
            ["style", "top", '535px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_E-02}": [
            ["style", "top", '322px'],
            ["style", "height", '127px'],
            ["style", "opacity", '0'],
            ["style", "left", '604px'],
            ["style", "width", '154px']
         ],
         "${_puzzle_Sb}": [
            ["style", "left", '0px']
         ],
         "${_Text-gokdelenCopy14}": [
            ["style", "top", '535px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_A-02}": [
            ["style", "top", '326px'],
            ["style", "height", '126px'],
            ["style", "opacity", '0'],
            ["style", "left", '408px'],
            ["style", "width", '157px']
         ],
         "${_Text-leylek}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_A-072}": [
            ["style", "top", '364px'],
            ["style", "height", '88px'],
            ["style", "opacity", '0'],
            ["style", "left", '289px'],
            ["style", "width", '136px']
         ],
         "${_menu}": [
            ["style", "top", '0px'],
            ["style", "border-bottom-left-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '52px'],
            ["color", "background-color", 'rgba(0,174,239,0.91)'],
            ["style", "left", '0px'],
            ["style", "width", '129px']
         ],
         "${_Text-gokdelenCopy22}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_icerigiCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "width", '124px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "cursor", 'pointer'],
            ["style", "font-size", '13px'],
            ["style", "top", '6px'],
            ["style", "display", 'none'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "left", '2px']
         ],
         "${_Puzzle_C-05}": [
            ["style", "top", '325px'],
            ["style", "height", '128px'],
            ["style", "opacity", '0'],
            ["style", "left", '408px'],
            ["style", "width", '157px']
         ],
         "${_RoundRect}": [
            ["style", "top", '697px'],
            ["style", "border-top-left-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "left", '0px'],
            ["style", "border-width", '1px'],
            ["style", "height", '56px'],
            ["style", "border-style", 'none'],
            ["style", "border-top-right-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "width", '983px']
         ],
         "${_Text-gokdelenCopy}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Turuncu_bant}": [
            ["style", "top", '0px'],
            ["subproperty", "boxShadow.color", 'rgba(41,41,41,0.65)'],
            ["subproperty", "boxShadow.blur", '4px'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["style", "left", '0px'],
            ["subproperty", "boxShadow.spread", '-2px']
         ],
         "${_Text3}": [
            ["style", "top", '753px'],
            ["style", "font-size", '8px'],
            ["style", "left", '786px'],
            ["color", "color", 'rgba(0,174,239,1.00)'],
            ["style", "height", '11px'],
            ["style", "cursor", 'auto'],
            ["style", "width", '197px']
         ],
         "${_Mouse_on2Copy2}": [
            ["style", "display", 'none'],
            ["style", "left", '550px'],
            ["style", "top", '470px']
         ],
         "${_Text5}": [
            ["style", "top", '711px'],
            ["style", "text-align", 'center'],
            ["style", "width", '925px'],
            ["style", "height", '35px'],
            ["style", "left", '28px'],
            ["style", "cursor", 'wait'],
            ["style", "font-size", '27px']
         ],
         "${_Rectangle2}": [
            ["style", "top", '25px'],
            ["style", "border-bottom-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-top-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '1px'],
            ["gradient", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]],
            ["style", "border-top-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "rotateZ", '-180deg']
         ],
         "${_Text-gokdelenCopy25}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy10}": [
            ["style", "top", '535px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_E-09}": [
            ["style", "top", '351px'],
            ["style", "height", '104px'],
            ["style", "opacity", '0'],
            ["style", "left", '394px'],
            ["style", "width", '128px']
         ],
         "${_seperation_Copy}": [
            ["style", "left", '565px'],
            ["style", "top", '452px']
         ],
         "${_Mouse_on2Copy}": [
            ["style", "display", 'none'],
            ["style", "left", '437px'],
            ["style", "top", '470px']
         ],
         "${_Kampanya_ButtonCopy}": [
            ["style", "top", '452px'],
            ["style", "border-bottom-left-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "height", '35px'],
            ["style", "cursor", 'context-menu'],
            ["style", "left", '306px'],
            ["style", "width", '136px']
         ],
         "${_Puzzle_C-13}": [
            ["style", "top", '354px'],
            ["style", "height", '98px'],
            ["style", "opacity", '0'],
            ["style", "left", '376px'],
            ["style", "width", '119px']
         ],
         "${_Text-gokdelenCopy19}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Text-gokdelenCopy21}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_amaci}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1px'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "width", '124px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Puzzle_B-07}": [
            ["style", "top", '374px'],
            ["style", "height", '79px'],
            ["style", "opacity", '0'],
            ["style", "left", '395px'],
            ["style", "width", '97px']
         ],
         "${_Puzzle_A-01}": [
            ["style", "top", '314px'],
            ["style", "height", '138px'],
            ["style", "opacity", '0'],
            ["style", "left", '395px'],
            ["style", "width", '172px']
         ],
         "${_Text-gokdelenCopy23}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_sloganimizCopy}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '567px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "width", '112px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Text-gokdelenCopy3}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_menuG}": [
            ["style", "display", 'none']
         ],
         "${_Kampanya_ButtonCopy3}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "border-bottom-right-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '35px'],
            ["style", "left", '566px'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '113px']
         ],
         "${_seperation_}": [
            ["style", "left", '441px'],
            ["style", "top", '452px']
         ],
         "${_Mouse_on1}": [
            ["style", "display", 'none'],
            ["style", "left", '311px'],
            ["style", "top", '470px']
         ],
         "${_Text-gokdelenCopy24}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_F-12}": [
            ["style", "top", '323px'],
            ["style", "height", '129px'],
            ["style", "opacity", '0'],
            ["style", "left", '439px'],
            ["style", "width", '162px']
         ],
         "${_Text-gokdelenCopy2}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ],
         "${_Puzzle_B-11}": [
            ["style", "top", '343px'],
            ["style", "height", '109px'],
            ["style", "opacity", '0'],
            ["style", "left", '322px'],
            ["style", "width", '136px']
         ],
         "${_Text-gokdelenCopy13}": [
            ["style", "top", '554px'],
            ["style", "text-align", 'left'],
            ["style", "height", '117px'],
            ["style", "opacity", '0.000000'],
            ["style", "left", '39px'],
            ["style", "font-size", '32px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 127601,
         autoPlay: true,
         labels: {
            "Basla": 0
         },
         timeline: [
            { id: "eid865", tween: [ "style", "${_Puzzle_D-06}", "top", '208px', { fromValue: '350px'}], position: 6000, duration: 2750 },
            { id: "eid291", tween: [ "style", "${_amaci}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid9", tween: [ "style", "${_Mouse_on1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid804", tween: [ "style", "${_Text-gokdelenCopy29}", "top", '535px', { fromValue: '535px'}], position: 122000, duration: 0 },
            { id: "eid794", tween: [ "style", "${_Text-gokdelenCopy25}", "opacity", '1', { fromValue: '0.000000'}], position: 104000, duration: 601 },
            { id: "eid795", tween: [ "style", "${_Text-gokdelenCopy25}", "opacity", '0', { fromValue: '1'}], position: 109000, duration: 601 },
            { id: "eid695", tween: [ "style", "${_Puzzle_F-12}", "top", '330px', { fromValue: '323px'}], position: 3250, duration: 2165 },
            { id: "eid805", tween: [ "style", "${_Text-gokdelenCopy29}", "opacity", '1', { fromValue: '0.000000'}], position: 122000, duration: 601 },
            { id: "eid806", tween: [ "style", "${_Text-gokdelenCopy29}", "opacity", '0', { fromValue: '1'}], position: 127000, duration: 601 },
            { id: "eid547", tween: [ "style", "${_icerigiCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid120", tween: [ "style", "${_RoundRect}", "width", '983px', { fromValue: '983px'}], position: 0, duration: 0 },
            { id: "eid852", tween: [ "style", "${_Puzzle_E-02}", "top", '268px', { fromValue: '322px'}], position: 5415, duration: 2585 },
            { id: "eid837", tween: [ "style", "${_Puzzle_B-11}", "left", '755px', { fromValue: '322px'}], position: 3885, duration: 2717 },
            { id: "eid553", tween: [ "style", "${_sloganimiz}", "left", '444px', { fromValue: '444px'}], position: 0, duration: 0 },
            { id: "eid844", tween: [ "style", "${_Puzzle_B-11}", "height", '79px', { fromValue: '109px'}], position: 3885, duration: 2717 },
            { id: "eid313", tween: [ "style", "${_Rectangle2}", "border-bottom-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid217", tween: [ "style", "${_Text5}", "font-size", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid825", tween: [ "style", "${_Puzzle_B-07}", "height", '79px', { fromValue: '79px'}], position: 4854, duration: 0 },
            { id: "eid701", tween: [ "style", "${_Text-gokdelen}", "opacity", '1', { fromValue: '0'}], position: 5000, duration: 1000 },
            { id: "eid707", tween: [ "style", "${_Text-gokdelen}", "opacity", '0', { fromValue: '1'}], position: 9500, duration: 500 },
            { id: "eid827", tween: [ "style", "${_Puzzle_B-07}", "left", '453px', { fromValue: '395px'}], position: 4854, duration: 2301 },
            { id: "eid649", tween: [ "style", "${_amaciCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid796", tween: [ "style", "${_Text-gokdelenCopy26}", "opacity", '1', { fromValue: '0.000000'}], position: 109000, duration: 601 },
            { id: "eid797", tween: [ "style", "${_Text-gokdelenCopy26}", "opacity", '0', { fromValue: '1'}], position: 117000, duration: 601 },
            { id: "eid172", tween: [ "style", "${_RoundRect}", "border-top-left-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid870", tween: [ "style", "${_Puzzle_D-06}", "height", '78px', { fromValue: '102px'}], position: 6000, duration: 2750 },
            { id: "eid677", tween: [ "style", "${_Puzzle_A-02}", "width", '97px', { fromValue: '157px'}], position: 1750, duration: 2878 },
            { id: "eid648", tween: [ "color", "${_amaciCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid926", tween: [ "style", "${_Puzzle_A-072}", "width", '96px', { fromValue: '136px'}], position: 7907, duration: 2843 },
            { id: "eid316", tween: [ "style", "${_menu}", "height", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid762", tween: [ "style", "${_Text-gokdelenCopy14}", "top", '535px', { fromValue: '535px'}], position: 64601, duration: 0 },
            { id: "eid304", tween: [ "style", "${_Rectangle2}", "height", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid284", tween: [ "style", "${_menu}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid907", tween: [ "style", "${_Puzzle_E-11}", "top", '252px', { fromValue: '339px'}], position: 6000, duration: 2750 },
            { id: "eid747", tween: [ "style", "${_Text-gokdelenCopy8}", "opacity", '1', { fromValue: '0.000000'}], position: 39000, duration: 601 },
            { id: "eid748", tween: [ "style", "${_Text-gokdelenCopy8}", "opacity", '0', { fromValue: '1'}], position: 44000, duration: 601 },
            { id: "eid550", tween: [ "color", "${_icerigiCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid753", tween: [ "style", "${_Text-gokdelenCopy10}", "top", '535px', { fromValue: '535px'}], position: 49601, duration: 0 },
            { id: "eid646", tween: [ "style", "${_amaciCopy}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid696", tween: [ "style", "${_Puzzle_F-12}", "height", '78px', { fromValue: '129px'}], position: 3250, duration: 2165 },
            { id: "eid784", tween: [ "style", "${_Text-gokdelenCopy20}", "opacity", '1', { fromValue: '0.000000'}], position: 79000, duration: 601 },
            { id: "eid785", tween: [ "style", "${_Text-gokdelenCopy20}", "opacity", '0', { fromValue: '1'}], position: 84000, duration: 601 },
            { id: "eid286", tween: [ "style", "${_icerigi}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid843", tween: [ "style", "${_Puzzle_B-11}", "width", '97px', { fromValue: '136px'}], position: 3885, duration: 2717 },
            { id: "eid818", tween: [ "style", "${_Puzzle_E-09}", "width", '97px', { fromValue: '128px'}], position: 4500, duration: 2355 },
            { id: "eid856", tween: [ "style", "${_Puzzle_E-02}", "opacity", '1', { fromValue: '0'}], position: 5415, duration: 1585 },
            { id: "eid933", tween: [ "style", "${_Puzzle_C-13}", "top", '130px', { fromValue: '354px'}], position: 7500, duration: 2913 },
            { id: "eid864", tween: [ "style", "${_Puzzle_D-06}", "left", '377px', { fromValue: '702px'}], position: 6000, duration: 2750 },
            { id: "eid330", tween: [ "style", "${_menuG}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid280", tween: [ "style", "${_icerigi}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid315", tween: [ "style", "${_menu}", "border-bottom-left-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid758", tween: [ "style", "${_Text-gokdelenCopy13}", "opacity", '1', { fromValue: '0.000000'}], position: 59000, duration: 601 },
            { id: "eid759", tween: [ "style", "${_Text-gokdelenCopy13}", "opacity", '0', { fromValue: '1'}], position: 64000, duration: 601 },
            { id: "eid667", tween: [ "style", "${_Puzzle_A-01}", "height", '63px', { fromValue: '138px'}], position: 0, duration: 3000 },
            { id: "eid851", tween: [ "style", "${_Puzzle_E-02}", "left", '55px', { fromValue: '604px'}], position: 5415, duration: 2585 },
            { id: "eid8", tween: [ "style", "${_Mouse_on2Copy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid213", tween: [ "style", "${_Text5}", "top", '711px', { fromValue: '711px'}], position: 0, duration: 0 },
            { id: "eid549", tween: [ "style", "${_icerigiCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid167", tween: [ "style", "${_RoundRect}", "top", '697px', { fromValue: '697px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_RoundRect}", "border-width", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid545", tween: [ "style", "${_icerigiCopy}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid751", tween: [ "style", "${_Text-gokdelenCopy10}", "opacity", '1', { fromValue: '0.000000'}], position: 49000, duration: 601 },
            { id: "eid752", tween: [ "style", "${_Text-gokdelenCopy10}", "opacity", '0', { fromValue: '1'}], position: 54000, duration: 601 },
            { id: "eid744", tween: [ "style", "${_Text-gokdelenCopy7}", "opacity", '1', { fromValue: '0.000000'}], position: 34000, duration: 500 },
            { id: "eid746", tween: [ "style", "${_Text-gokdelenCopy7}", "opacity", '0', { fromValue: '1'}], position: 39000, duration: 601 },
            { id: "eid828", tween: [ "style", "${_Puzzle_B-07}", "top", '69px', { fromValue: '374px'}], position: 4854, duration: 2301 },
            { id: "eid824", tween: [ "style", "${_Puzzle_B-07}", "width", '97px', { fromValue: '97px'}], position: 4854, duration: 0 },
            { id: "eid697", tween: [ "style", "${_Puzzle_F-12}", "width", '98px', { fromValue: '162px'}], position: 3250, duration: 2165 },
            { id: "eid858", tween: [ "style", "${_Puzzle_E-02}", "width", '96px', { fromValue: '154px'}], position: 5415, duration: 2585 },
            { id: "eid662", tween: [ "style", "${_Puzzle_A-01}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 2000 },
            { id: "eid668", tween: [ "style", "${_Puzzle_A-01}", "width", '77px', { fromValue: '172px'}], position: 0, duration: 3000 },
            { id: "eid899", tween: [ "style", "${_Puzzle_F-04}", "height", '78px', { fromValue: '117px'}], position: 7000, duration: 2750 },
            { id: "eid821", tween: [ "style", "${_Puzzle_E-09}", "opacity", '1', { fromValue: '0'}], position: 4500, duration: 1750 },
            { id: "eid869", tween: [ "style", "${_Puzzle_D-06}", "opacity", '1', { fromValue: '0'}], position: 6000, duration: 1500 },
            { id: "eid906", tween: [ "style", "${_Puzzle_E-11}", "left", '735px', { fromValue: '152px'}], position: 6000, duration: 2750 },
            { id: "eid912", tween: [ "style", "${_Puzzle_E-11}", "height", '79px', { fromValue: '116px'}], position: 6000, duration: 2750 },
            { id: "eid882", tween: [ "style", "${_Puzzle_C-09}", "opacity", '1', { fromValue: '0'}], position: 6500, duration: 1250 },
            { id: "eid838", tween: [ "style", "${_Puzzle_B-11}", "top", '68px', { fromValue: '343px'}], position: 3885, duration: 2717 },
            { id: "eid119", tween: [ "style", "${_RoundRect}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid299", tween: [ "transform", "${_Rectangle2}", "rotateZ", '-180deg', { fromValue: '-180deg'}], position: 0, duration: 0 },
            { id: "eid857", tween: [ "style", "${_Puzzle_E-02}", "height", '79px', { fromValue: '127px'}], position: 5415, duration: 2585 },
            { id: "eid538", tween: [ "style", "${_Text5}", "left", '28px', { fromValue: '28px'}], position: 0, duration: 0 },
            { id: "eid728", tween: [ "style", "${_Text-gokdelenCopy2}", "opacity", '1', { fromValue: '0.000000'}], position: 14000, duration: 500 },
            { id: "eid730", tween: [ "style", "${_Text-gokdelenCopy2}", "opacity", '0', { fromValue: '1'}], position: 19000, duration: 500 },
            { id: "eid687", tween: [ "style", "${_Puzzle_C-05}", "width", '97px', { fromValue: '157px'}], position: 1370, duration: 2380 },
            { id: "eid938", tween: [ "style", "${_Puzzle_C-13}", "height", '79px', { fromValue: '98px'}], position: 7500, duration: 2913 },
            { id: "eid877", tween: [ "style", "${_Puzzle_C-09}", "left", '584px', { fromValue: '169px'}], position: 6500, duration: 2640 },
            { id: "eid749", tween: [ "style", "${_Text-gokdelenCopy9}", "opacity", '1', { fromValue: '0.000000'}], position: 44000, duration: 601 },
            { id: "eid750", tween: [ "style", "${_Text-gokdelenCopy9}", "opacity", '0', { fromValue: '1'}], position: 49000, duration: 601 },
            { id: "eid735", tween: [ "style", "${_Text-gokdelenCopy5}", "opacity", '1', { fromValue: '0.000000'}], position: 29000, duration: 500 },
            { id: "eid736", tween: [ "style", "${_Text-gokdelenCopy5}", "opacity", '0', { fromValue: '1'}], position: 34000, duration: 500 },
            { id: "eid104", tween: [ "style", "${_sloganimiz}", "top", '461px', { fromValue: '461px'}], position: 0, duration: 0 },
            { id: "eid896", tween: [ "style", "${_Puzzle_F-04}", "top", '330px', { fromValue: '338px'}], position: 7000, duration: 2750 },
            { id: "eid733", tween: [ "style", "${_Text-gokdelenCopy4}", "opacity", '1', { fromValue: '0.000000'}], position: 24000, duration: 500 },
            { id: "eid734", tween: [ "style", "${_Text-gokdelenCopy4}", "opacity", '0', { fromValue: '1'}], position: 29000, duration: 500 },
            { id: "eid54", tween: [ "style", "${_Text3}", "font-size", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid900", tween: [ "style", "${_Puzzle_F-04}", "width", '98px', { fromValue: '147px'}], position: 7000, duration: 2750 },
            { id: "eid830", tween: [ "style", "${_Puzzle_B-07}", "opacity", '1', { fromValue: '0'}], position: 4854, duration: 2001 },
            { id: "eid801", tween: [ "style", "${_Text-gokdelenCopy28}", "opacity", '1', { fromValue: '0.000000'}], position: 117000, duration: 601 },
            { id: "eid802", tween: [ "style", "${_Text-gokdelenCopy28}", "opacity", '0', { fromValue: '1'}], position: 122000, duration: 601 },
            { id: "eid675", tween: [ "style", "${_Puzzle_A-02}", "top", '25px', { fromValue: '326px'}], position: 1750, duration: 2878 },
            { id: "eid214", tween: [ "style", "${_Text5}", "height", '35px', { fromValue: '35px'}], position: 0, duration: 0 },
            { id: "eid554", tween: [ "style", "${_sloganimiz}", "width", '121px', { fromValue: '121px'}], position: 0, duration: 0 },
            { id: "eid939", tween: [ "style", "${_Puzzle_C-13}", "width", '96px', { fromValue: '119px'}], position: 7500, duration: 2913 },
            { id: "eid546", tween: [ "style", "${_icerigiCopy}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid932", tween: [ "style", "${_Puzzle_C-13}", "left", '887px', { fromValue: '376px'}], position: 7500, duration: 2913 },
            { id: "eid645", tween: [ "style", "${_amaciCopy}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid117", tween: [ "style", "${_RoundRect}", "height", '56px', { fromValue: '56px'}], position: 0, duration: 0 },
            { id: "eid798", tween: [ "style", "${_Text-gokdelenCopy26}", "top", '517px', { fromValue: '517px'}], position: 109601, duration: 0 },
            { id: "eid698", tween: [ "style", "${_Puzzle_F-12}", "opacity", '1', { fromValue: '0'}], position: 3250, duration: 1750 },
            { id: "eid559", tween: [ "style", "${_Text3}", "width", '197px', { fromValue: '197px'}], position: 0, duration: 0 },
            { id: "eid786", tween: [ "style", "${_Text-gokdelenCopy21}", "opacity", '1', { fromValue: '0.000000'}], position: 84000, duration: 601 },
            { id: "eid787", tween: [ "style", "${_Text-gokdelenCopy21}", "opacity", '0', { fromValue: '1'}], position: 89000, duration: 601 },
            { id: "eid884", tween: [ "style", "${_Puzzle_C-09}", "width", '96px', { fromValue: '119px'}], position: 6500, duration: 2640 },
            { id: "eid731", tween: [ "style", "${_Text-gokdelenCopy3}", "opacity", '1', { fromValue: '0.000000'}], position: 19000, duration: 500 },
            { id: "eid732", tween: [ "style", "${_Text-gokdelenCopy3}", "opacity", '0', { fromValue: '1'}], position: 24000, duration: 500 },
            { id: "eid700", tween: [ "style", "${_Text-leylek}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 1000 },
            { id: "eid703", tween: [ "style", "${_Text-leylek}", "opacity", '0', { fromValue: '1'}], position: 5000, duration: 500 },
            { id: "eid560", tween: [ "style", "${_Text3}", "left", '786px', { fromValue: '786px'}], position: 0, duration: 0 },
            { id: "eid760", tween: [ "style", "${_Text-gokdelenCopy14}", "opacity", '1', { fromValue: '0.000000'}], position: 64000, duration: 601 },
            { id: "eid761", tween: [ "style", "${_Text-gokdelenCopy14}", "opacity", '0', { fromValue: '1'}], position: 69000, duration: 601 },
            { id: "eid647", tween: [ "style", "${_amaciCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid717", tween: [ "style", "${_Text-gokdelenCopy}", "opacity", '1', { fromValue: '0.000000'}], position: 9500, duration: 500 },
            { id: "eid719", tween: [ "style", "${_Text-gokdelenCopy}", "opacity", '0', { fromValue: '1'}], position: 14000, duration: 500 },
            { id: "eid544", tween: [ "style", "${_amaci}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid803", tween: [ "style", "${_Text-gokdelenCopy28}", "top", '535px', { fromValue: '535px'}], position: 117000, duration: 0 },
            { id: "eid270", tween: [ "style", "${_menu}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid913", tween: [ "style", "${_Puzzle_E-11}", "width", '96px', { fromValue: '141px'}], position: 6000, duration: 2750 },
            { id: "eid780", tween: [ "style", "${_Text-gokdelenCopy18}", "opacity", '1', { fromValue: '0.000000'}], position: 69000, duration: 601 },
            { id: "eid781", tween: [ "style", "${_Text-gokdelenCopy18}", "opacity", '0', { fromValue: '1'}], position: 74000, duration: 601 },
            { id: "eid756", tween: [ "style", "${_Text-gokdelenCopy12}", "opacity", '1', { fromValue: '0.000000'}], position: 54000, duration: 601 },
            { id: "eid757", tween: [ "style", "${_Text-gokdelenCopy12}", "opacity", '0', { fromValue: '1'}], position: 59000, duration: 601 },
            { id: "eid937", tween: [ "style", "${_Puzzle_C-13}", "opacity", '1', { fromValue: '0'}], position: 7500, duration: 1500 },
            { id: "eid311", tween: [ "style", "${_Rectangle2}", "border-top-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid555", tween: [ "style", "${_kampanya_hakkinda}", "width", '128px', { fromValue: '128px'}], position: 0, duration: 0 },
            { id: "eid685", tween: [ "style", "${_Puzzle_C-05}", "top", '130px', { fromValue: '325px'}], position: 1370, duration: 2380 },
            { id: "eid919", tween: [ "style", "${_Puzzle_A-072}", "left", '433px', { fromValue: '289px'}], position: 7907, duration: 2843 },
            { id: "eid817", tween: [ "style", "${_Puzzle_E-09}", "top", '252px', { fromValue: '351px'}], position: 4500, duration: 2355 },
            { id: "eid674", tween: [ "style", "${_Puzzle_A-02}", "left", '55px', { fromValue: '408px'}], position: 1750, duration: 2878 },
            { id: "eid666", tween: [ "style", "${_Puzzle_A-01}", "top", '25px', { fromValue: '314px'}], position: 0, duration: 3000 },
            { id: "eid314", tween: [ "style", "${_menu}", "border-bottom-right-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid738", tween: [ "style", "${_Text-gokdelenCopy5}", "top", '535px', { fromValue: '535px'}], position: 29000, duration: 0 },
            { id: "eid678", tween: [ "style", "${_Puzzle_A-02}", "opacity", '1', { fromValue: '0'}], position: 1750, duration: 2000 },
            { id: "eid924", tween: [ "style", "${_Puzzle_A-072}", "opacity", '1', { fromValue: '0'}], position: 7907, duration: 1465 },
            { id: "eid558", tween: [ "style", "${_Text3}", "top", '753px', { fromValue: '753px'}], position: 0, duration: 0 },
            { id: "eid883", tween: [ "style", "${_Puzzle_C-09}", "height", '79px', { fromValue: '98px'}], position: 6500, duration: 2640 },
            { id: "eid7", tween: [ "style", "${_Mouse_on2Copy2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid676", tween: [ "style", "${_Puzzle_A-02}", "height", '78px', { fromValue: '126px'}], position: 1750, duration: 2878 },
            { id: "eid686", tween: [ "style", "${_Puzzle_C-05}", "height", '79px', { fromValue: '128px'}], position: 1370, duration: 2380 },
            { id: "eid694", tween: [ "style", "${_Puzzle_F-12}", "left", '830px', { fromValue: '439px'}], position: 3250, duration: 2165 },
            { id: "eid878", tween: [ "style", "${_Puzzle_C-09}", "top", '130px', { fromValue: '361px'}], position: 6500, duration: 2640 },
            { id: "eid171", tween: [ "style", "${_RoundRect}", "border-top-right-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid684", tween: [ "style", "${_Puzzle_C-05}", "left", '282px', { fromValue: '408px'}], position: 1370, duration: 2380 },
            { id: "eid322", tween: [ "style", "${_icerigi}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid788", tween: [ "style", "${_Text-gokdelenCopy22}", "opacity", '1', { fromValue: '0.000000'}], position: 89000, duration: 601 },
            { id: "eid789", tween: [ "style", "${_Text-gokdelenCopy22}", "opacity", '0', { fromValue: '1'}], position: 94000, duration: 601 },
            { id: "eid271", tween: [ "style", "${_menu}", "width", '129px', { fromValue: '129px'}], position: 0, duration: 0 },
            { id: "eid310", tween: [ "style", "${_Rectangle2}", "border-bottom-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid551", tween: [ "style", "${_kampanya_hakkinda}", "left", '311px', { fromValue: '311px'}], position: 0, duration: 0 },
            { id: "eid790", tween: [ "style", "${_Text-gokdelenCopy23}", "opacity", '1', { fromValue: '0.000000'}], position: 94000, duration: 601 },
            { id: "eid791", tween: [ "style", "${_Text-gokdelenCopy23}", "opacity", '0', { fromValue: '1'}], position: 99000, duration: 601 },
            { id: "eid792", tween: [ "style", "${_Text-gokdelenCopy24}", "opacity", '1', { fromValue: '0.000000'}], position: 99000, duration: 601 },
            { id: "eid793", tween: [ "style", "${_Text-gokdelenCopy24}", "opacity", '0', { fromValue: '1'}], position: 104000, duration: 601 },
            { id: "eid59", tween: [ "color", "${_Text3}", "color", 'rgba(0,174,239,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1.00)'}], position: 0, duration: 0 },
            { id: "eid819", tween: [ "style", "${_Puzzle_E-09}", "height", '79px', { fromValue: '104px'}], position: 4500, duration: 2355 },
            { id: "eid307", tween: [ "gradient", "${_Rectangle2}", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]], { fromValue: [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]}], position: 0, duration: 0 },
            { id: "eid871", tween: [ "style", "${_Puzzle_D-06}", "width", '98px', { fromValue: '128px'}], position: 6000, duration: 2750 },
            { id: "eid319", tween: [ "style", "${_amaci}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_Text3}", "height", '11px', { fromValue: '11px'}], position: 0, duration: 0 },
            { id: "eid842", tween: [ "style", "${_Puzzle_B-11}", "opacity", '1', { fromValue: '0'}], position: 3885, duration: 1365 },
            { id: "eid312", tween: [ "style", "${_Rectangle2}", "border-top-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid782", tween: [ "style", "${_Text-gokdelenCopy19}", "opacity", '1', { fromValue: '0.000000'}], position: 74000, duration: 601 },
            { id: "eid783", tween: [ "style", "${_Text-gokdelenCopy19}", "opacity", '0', { fromValue: '1'}], position: 79000, duration: 601 },
            { id: "eid665", tween: [ "style", "${_Puzzle_A-01}", "left", '0px', { fromValue: '395px'}], position: 0, duration: 3000 },
            { id: "eid920", tween: [ "style", "${_Puzzle_A-072}", "top", '25px', { fromValue: '364px'}], position: 7907, duration: 2843 },
            { id: "eid288", tween: [ "color", "${_menu}", "background-color", 'rgba(0,174,239,0.91)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,0.91)'}], position: 0, duration: 0 },
            { id: "eid911", tween: [ "style", "${_Puzzle_E-11}", "opacity", '1', { fromValue: '0'}], position: 6000, duration: 1250 },
            { id: "eid320", tween: [ "style", "${_Rectangle2}", "top", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid898", tween: [ "style", "${_Puzzle_F-04}", "opacity", '1', { fromValue: '0'}], position: 7000, duration: 1620 },
            { id: "eid688", tween: [ "style", "${_Puzzle_C-05}", "opacity", '1', { fromValue: '0'}], position: 1370, duration: 1880 },
            { id: "eid895", tween: [ "style", "${_Puzzle_F-04}", "left", '226px', { fromValue: '717px'}], position: 7000, duration: 2750 },
            { id: "eid925", tween: [ "style", "${_Puzzle_A-072}", "height", '62px', { fromValue: '88px'}], position: 7907, duration: 2843 },
            { id: "eid816", tween: [ "style", "${_Puzzle_E-09}", "left", '583px', { fromValue: '394px'}], position: 4500, duration: 2355 },
            { id: "eid556", tween: [ "style", "${_Text5}", "width", '925px', { fromValue: '925px'}], position: 0, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'you_tube_butt',
      type: 'image',
      rect: ['0px','0px','72px','50px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/you_tube_butt.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '50px'],
            ["style", "width", '72px']
         ],
         "${_you_tube_butt}": [
            ["style", "top", '0px'],
            ["style", "height", '50px'],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["style", "width", '72px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: false,
         timeline: [
            { id: "eid17", tween: [ "style", "${_you_tube_butt}", "top", '-7px', { fromValue: '0px'}], position: 0, duration: 500 },
            { id: "eid21", tween: [ "style", "${_you_tube_butt}", "opacity", '0.53', { fromValue: '1'}], position: 0, duration: 500 },
            { id: "eid18", tween: [ "style", "${_you_tube_butt}", "width", '92px', { fromValue: '72px'}], position: 0, duration: 500 },
            { id: "eid16", tween: [ "style", "${_you_tube_butt}", "height", '64px', { fromValue: '50px'}], position: 0, duration: 500 },
            { id: "eid19", tween: [ "style", "${_you_tube_butt}", "left", '-10px', { fromValue: '0px'}], position: 0, duration: 500 }         ]
      }
   }
},
"tamambutton": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['-13px','34px','72px','34px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'RoundRect2',
      stroke: [1,'rgb(0, 0, 0)','solid'],
      type: 'rect',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_RoundRect2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '0px'],
            ["style", "border-width", '3px'],
            ["style", "border-style", 'solid'],
            ["style", "height", '27px'],
            ["color", "border-color", 'rgba(150,150,150,0.60)'],
            ["style", "left", '0px'],
            ["style", "width", '73px']
         ],
         "${symbolSelector}": [
            ["style", "height", '33px'],
            ["style", "width", '79px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 250,
         autoPlay: false,
         timeline: [
            { id: "eid98", tween: [ "style", "${_RoundRect2}", "height", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid175", tween: [ "style", "${_RoundRect2}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid99", tween: [ "style", "${_RoundRect2}", "width", '73px', { fromValue: '73px'}], position: 0, duration: 0 },
            { id: "eid174", tween: [ "style", "${_RoundRect2}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid173", tween: [ "color", "${_RoundRect2}", "border-color", 'rgba(150,150,150,0.60)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(150,150,150,0.60)'}], position: 0, duration: 0 },
            { id: "eid189", tween: [ "color", "${_RoundRect2}", "background-color", 'rgba(0,104,143,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1)'}], position: 0, duration: 250 },
            { id: "eid100", tween: [ "style", "${_RoundRect2}", "border-width", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
},
"ic_button": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'sol_ic',
      type: 'image',
      rect: ['-3px','-10px','40px','40px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/sol_ic.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_sol_ic}": [
            ["style", "top", '0px'],
            ["transform", "scaleY", '1'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '0.60037267208099'],
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '40px'],
            ["style", "width", '40px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 192.03515625,
         autoPlay: false,
         timeline: [
            { id: "eid352", tween: [ "style", "${_sol_ic}", "opacity", '0.38604798913002', { fromValue: '0.6003730297088623'}], position: 0, duration: 192 },
            { id: "eid341", tween: [ "style", "${_sol_ic}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid349", tween: [ "transform", "${_sol_ic}", "scaleY", '1.21813', { fromValue: '1'}], position: 0, duration: 192 },
            { id: "eid340", tween: [ "style", "${_sol_ic}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid348", tween: [ "transform", "${_sol_ic}", "scaleX", '1.21813', { fromValue: '1'}], position: 0, duration: 192 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-91745653");
