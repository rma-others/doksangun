/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Ban_Zemin',
            type:'image',
            rect:['0px','25px','983px','427px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Ban_Zemin.jpg",'0px','0px']
         },
         {
            id:'Kampanya_ButtonCopy',
            type:'rect',
            rect:['306px','452px','136px','35px','auto','auto'],
            cursor:['context-menu'],
            borderRadius:["0px","0px","0px","9px 9px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy2',
            type:'rect',
            rect:['442px','452px','124px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","0px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy3',
            type:'rect',
            rect:['566px','452px','113px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","9px 9px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'seperation_',
            type:'image',
            rect:['441px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'seperation_Copy',
            type:'image',
            rect:['565px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'Mouse_on1',
            display:'none',
            type:'image',
            rect:['311px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy',
            display:'none',
            type:'image',
            rect:['437px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy2',
            display:'none',
            type:'image',
            rect:['550px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'kampanya_hakkinda',
            type:'text',
            rect:['306px','462px','140px','17px','auto','auto'],
            cursor:['pointer'],
            text:"Çalışma hakkında",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimiz',
            type:'text',
            rect:['448px','462px','106px','17px','auto','auto'],
            cursor:['pointer'],
            text:"90 Gün Teması",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimizCopy',
            type:'text',
            rect:['567px','462px','112px','17px','auto','auto'],
            cursor:['pointer'],
            text:"Bize ulaşın",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'Video1',
            type:'image',
            rect:['0px','501px','318px','180px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Video1.jpg",'0px','0px'],
            boxShadow:["",3,3,3,0,"rgba(0,0,0,0.65)"]
         },
         {
            id:'Symbol_12',
            type:'rect',
            rect:['117','559','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'Rectangle',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'RectangleCopy',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Text',
            type:'text',
            rect:['697px','412px','10px','17px','auto','auto'],
            text:"Beni Haberdar Et",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',24,"rgba(0,0,0,1)","normal","none",""]
         },
         {
            id:'TextCopy',
            type:'text',
            rect:['697px','412px','10px','17px','auto','auto'],
            text:"Günün Mesajı",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',24,"rgba(0,0,0,1)","normal","none",""]
         },
         {
            id:'Text2',
            type:'text',
            rect:['671px','634px','312px','84px','auto','auto'],
            text:"90 Gün Çalışması ve Türkiye Kamu Hastaneleri'ne ilişkin bilgi almak için\"Tamam\" butonunu tıklayarak e-bülten'e üye olabilirsiniz.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',24,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'Text2Copy',
            type:'text',
            rect:['671px','634px','312px','84px','auto','auto'],
            text:"Hedefler, başarıya giden yoldaki ölçülebilir kilometre taşlarıdır.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',24,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'Text3',
            type:'text',
            rect:['877','768','auto','auto','auto','auto'],
            text:"©Copyright 2013 Türkiye Kamu Hastaneleri Kurumu",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',15,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'tamambutton',
            type:'rect',
            rect:['887','626','auto','auto','auto','auto']
         },
         {
            id:'Text4',
            type:'text',
            rect:['898','643','auto','auto','auto','auto'],
            cursor:['pointer'],
            text:"Tamam",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',8,"rgba(0,174,239,1)","500","none","normal"]
         },
         {
            id:'RoundRect',
            type:'rect',
            rect:['1px','701px','977px','56px','auto','auto'],
            fill:["rgba(0,174,239,1)"],
            stroke:[1,"rgb(150, 150, 150)","none"]
         },
         {
            id:'Text5',
            type:'text',
            rect:['57','710','auto','auto','auto','auto'],
            cursor:['wait'],
            text:"90 Gün Çalışması'nın tamamlanmasına 80 Gün kaldı.",
            align:"center",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',14,"rgba(255,255,255,1)","500","none","normal"]
         },
         {
            id:'menuG',
            display:'none',
            type:'group',
            rect:['318','487','129','49','auto','auto'],
            c:[
            {
               id:'menu',
               type:'rect',
               rect:['6px','0px','124px','50px','auto','auto'],
               fill:["rgba(192,192,192,1)"],
               stroke:[0,"rgba(0,0,0,1)","none"]
            },
            {
               id:'Rectangle2',
               type:'rect',
               rect:['0px','23px','123px','3px','auto','auto'],
               fill:["rgba(0,174,239,0.9063)"],
               stroke:[0,"rgb(0, 0, 0)","none"]
            },
            {
               id:'amaci',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'amaciCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigi',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigiCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            }]
         },
         {
            id:'zemin_kose2',
            type:'image',
            rect:['0','25','334px','424px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"zemin_kose.png",'0px','0px']
         },
         {
            id:'Turuncu_bant',
            type:'image',
            rect:['0px','0px','983px','25px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Turuncu_bant.png",'0px','0px'],
            boxShadow:["",0,3,4,-2,"rgba(0,0,0,0.65)"]
         },
         {
            id:'_90Gun_text012',
            type:'image',
            rect:['351','130','486px','238px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"_90Gun_text01.png",'0px','0px']
         },
         {
            id:'_90Gun_text02',
            type:'image',
            rect:['997px','144px','482px','202px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"_90Gun_text02.png",'0px','0px']
         },
         {
            id:'_90Gun_text03Copy',
            type:'image',
            rect:['997px','144','503px','208px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"_90Gun_text03.png",'0px','0px']
         },
         {
            id:'_90Gun_text04',
            type:'image',
            rect:['983px','132px','611px','204px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"_90Gun_text04.png",'0px','0px']
         },
         {
            id:'RectangleCopy2',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'RectangleCopy3',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'damga2',
            type:'image',
            rect:['767','346','152px','158px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"damga.png",'0px','0px']
         },
         {
            id:'SB-TKHK',
            type:'image',
            rect:['840','25','98px','106px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"SB-TKHK.png",'0px','0px']
         },
         {
            id:'icerikText01',
            type:'text',
            rect:['28px','112px','823px','290px','auto','auto'],
            clip:['rect(0px 823px 304px 0px)'],
            text:"Dünya’da 20. yüzyılın son çeyreği ve 21. yüzyılın başlarında yaşanan hızlı değişim ve gelişmeler, küreselleşmenin sonuçları, bilginin üretim ve tüketim hızının artması gibi birçok faktör, tüm sektörler gibi sağlık sektörünü de etkilemiş ve uyum sağlama sürecinin başlamasını zorunlu kılmıştır.<br><br>Bu süreçte T.C. Sağlık Bakanlığı’mızın ‘Önce İnsan’ diyerek başlattığı Sağlıkta Dönüşüm Programı ile önemli adımlar atılmıştır. Hedeflenen Türkiye Cumhuriyeti Devleti’nin sağlık sistemini 21. yüzyıl vizyonuna uygun hale getirmek ve vatandaşlarımıza hakları olan kaliteli sağlık hizmetini sunmak konusunda aralıksız çalışmaktır. Bu çalışmalar kapsamında merkeziyetçi yönetim yapısından uzaklaşılmış yerinden yönetim modeline geçilmiş ve bu amaçla Türkiye Kamu Hastaneleri Kurumu’nun il düzeyindeki yönetim teşkilatı olan İl Genel Sekreterlikleri yapılandırılmıştır.<br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',21,"rgba(0,0,0,1.00)","400","none","normal"],
            transform:[]
         },
         {
            id:'Text6',
            display:'none',
            type:'text',
            rect:['29','52px','auto','auto','auto','auto'],
            text:"90 GÜN ",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',29,"rgba(0,174,239,1.00)","normal","none","normal"]
         },
         {
            id:'RoundRect2',
            type:'rect',
            rect:['440px','427px','124px','17px','auto','auto'],
            borderRadius:["10px","10px","10px","10px"],
            fill:["rgba(218,218,218,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         },
         {
            id:'Ellipse',
            type:'ellipse',
            rect:['449px','430px','12px','11px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["50%","50%","50%","50%"],
            fill:["rgba(104,209,246,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'EllipseCopy4',
            type:'ellipse',
            rect:['495px','430px','12px','11px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["50%","50%","50%","50%"],
            fill:["rgba(178,178,178,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'EllipseCopy',
            type:'ellipse',
            rect:['495px','430px','12px','11px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["50%","50%","50%","50%"],
            fill:["rgba(178,178,178,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Text8',
            type:'text',
            rect:['29px','94px','811px','306px','auto','auto'],
            text:"90 gün çalışması, ‘hedeflerle yönetim ilkesine‘ dayanan bir yönetim çalışmasıdır. Çalışma kapsamında 90 gün gibi bir sürede İl Genel Sekreterlikleri’nin ve Merkez Yönetim Teşkilatı’nın koordineli çalışmaları sonucu kamu hastanelerine ilişkin bazı yeniliklerin hayata geçirilmesi hedeflenmektedir. Çalışmanın tamamlanma süresi 90 gündür. Bu nedenle çalışma ‘90 Gün Çalışması’ olarak adlandırılmıştır. <br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',21,"rgba(26,77,105,1)","normal","none","normal"]
         },
         {
            id:'Text9',
            display:'none',
            type:'text',
            rect:['29px','105px','811px','306px','auto','auto'],
            text:"Daha etkin bir yönetişim, hasta memnuniyetini ve kamu hastanelerinde görev yapan sağlık çalışanlarının memnuniyetini artıracak adımların atılması olmak üzere üç ana hedefimiz mevcuttur. <br>Bu çalışmanın sona ermesinin ardından yeni 90 günler başlayacak ve yeni çalışmalar hızla hayata geçecektir.<br>Genel sekreterler ve merkez yönetim teşkilatımızın koordineli olarak ekip ruhuyla çalışacağına inandığım bu çalışma için hepinize başarılar diler ve her an yanınızda olduğumu unutmamanızı temenni ederim.<br><br><br><br>",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',21,"rgba(26,77,105,1)","normal","none","normal"]
         },
         {
            id:'Text11',
            display:'none',
            type:'text',
            rect:['29px','321px','794px','39px','auto','auto'],
            text:"‘Kararlı bir yaklaşımla ve hizmete inanmış kişilerle, 90 günde bir gökdelen inşa etmek mümkündür.’",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',16,"rgba(26,77,105,1)","normal","none","italic"]
         },
         {
            id:'Text10',
            display:'none',
            type:'text',
            rect:['435px','356px','341px','84px','auto','auto'],
            text:"Dr. Hasan Çağıl<br>Kamu Hastaneleri Kurumu Başkanı",
            align:"center",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',21,"rgba(26,77,105,1)","normal","none","normal"]
         }],
         symbolInstances: [
         {
            id:'Symbol_12',
            symbolName:'Symbol_1'
         },
         {
            id:'tamambutton',
            symbolName:'tamambutton'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Rectangle2}": [
            ["style", "top", '25px'],
            ["style", "border-top-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "rotateZ", '-180deg'],
            ["style", "border-bottom-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '1px'],
            ["gradient", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]],
            ["style", "border-top-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_Text2Copy}": [
            ["style", "top", '550px'],
            ["style", "font-weight", '500'],
            ["style", "left", '338px'],
            ["style", "font-size", '15px']
         ],
         "${_icerikText01}": [
            ["color", "color", 'rgba(0,25,39,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '28px'],
            ["style", "font-size", '21px'],
            ["style", "top", '94px'],
            ["style", "width", '823px'],
            ["style", "text-align", 'left'],
            ["transform", "scaleY", '1'],
            ["style", "display", 'block'],
            ["transform", "scaleX", '1'],
            ["style", "clip", [0,823,304,0], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "opacity", '0']
         ],
         "${_sloganimiz}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '444px'],
            ["style", "font-size", '13px'],
            ["style", "top", '461px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '121px']
         ],
         "${_Text2}": [
            ["style", "top", '550px'],
            ["style", "height", '93px'],
            ["style", "font-weight", '500'],
            ["style", "left", '671px'],
            ["style", "font-size", '15px']
         ],
         "${_kampanya_hakkinda}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '311px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '128px']
         ],
         "${_zemin_kose2}": [
            ["style", "opacity", '0.45']
         ],
         "${_Text4}": [
            ["style", "top", '635px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '891px'],
            ["style", "font-size", '14px']
         ],
         "${_amaciCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "width", '124px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1px'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "height", '17px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_RoundRect2}": [
            ["color", "background-color", 'rgba(218,218,218,1.00)'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["style", "left", '454px'],
            ["style", "width", '94px']
         ],
         "${_menu}": [
            ["style", "top", '0px'],
            ["style", "border-bottom-left-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '52px'],
            ["color", "background-color", 'rgba(0,174,239,0.91)'],
            ["style", "left", '0px'],
            ["style", "width", '129px']
         ],
         "${_Text10}": [
            ["style", "top", '348px'],
            ["style", "text-align", 'center'],
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["style", "left", '435px'],
            ["style", "width", '341px']
         ],
         "${_SB-TKHK}": [
            ["style", "left", '859px'],
            ["style", "top", '38px']
         ],
         "${_amaci}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1px'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '124px']
         ],
         "${_Mouse_on2Copy2}": [
            ["style", "top", '470px'],
            ["style", "left", '550px'],
            ["style", "display", 'none']
         ],
         "${_Ellipse}": [
            ["color", "background-color", 'rgba(104,209,246,1.00)'],
            ["style", "top", '430px'],
            ["style", "left", '463px'],
            ["style", "display", 'block'],
            ["style", "height", '11px'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '12px']
         ],
         "${__90Gun_text012}": [
            ["style", "top", '192px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '124px'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '0'],
            ["style", "left", '486px'],
            ["style", "width", '212px']
         ],
         "${_EllipseCopy4}": [
            ["color", "background-color", 'rgba(178,178,178,1.00)'],
            ["style", "top", '430px'],
            ["style", "left", '494px'],
            ["style", "display", 'block'],
            ["style", "height", '11px'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '12px']
         ],
         "${_Text9}": [
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["style", "height", '224px']
         ],
         "${_TextCopy}": [
            ["style", "top", '502px'],
            ["style", "font-size", '23px'],
            ["style", "height", '25px'],
            ["color", "color", 'rgba(0,25,39,1.00)'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "left", '338px'],
            ["style", "width", '196px']
         ],
         "${_Symbol_12}": [
            ["style", "top", '567px'],
            ["style", "left", '123px'],
            ["style", "cursor", 'pointer']
         ],
         "${_sloganimizCopy}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '567px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '112px']
         ],
         "${_RoundRect}": [
            ["style", "top", '697px'],
            ["style", "border-top-left-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "left", '0px'],
            ["style", "border-top-right-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '56px'],
            ["style", "border-style", 'none'],
            ["style", "border-width", '1px'],
            ["style", "width", '983px']
         ],
         "${_tamambutton}": [
            ["style", "left", '874px']
         ],
         "${_Text11}": [
            ["style", "top", '321px'],
            ["style", "width", '794px'],
            ["style", "height", '39px'],
            ["style", "font-style", 'italic'],
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["style", "left", '29px'],
            ["style", "font-size", '16px']
         ],
         "${_Text3}": [
            ["style", "top", '753px'],
            ["style", "width", '197px'],
            ["style", "cursor", 'auto'],
            ["color", "color", 'rgba(0,174,239,1.00)'],
            ["style", "height", '11px'],
            ["style", "left", '786px'],
            ["style", "font-size", '8px']
         ],
         "${_Turuncu_bant}": [
            ["style", "top", '0px'],
            ["subproperty", "boxShadow.blur", '4px'],
            ["subproperty", "boxShadow.spread", '-2px'],
            ["style", "left", '0px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["subproperty", "boxShadow.color", 'rgba(41,41,41,0.65)']
         ],
         "${_Text5}": [
            ["style", "top", '711px'],
            ["style", "text-align", 'center'],
            ["style", "font-size", '27px'],
            ["style", "height", '35px'],
            ["style", "cursor", 'wait'],
            ["style", "left", '28px'],
            ["style", "width", '925px']
         ],
         "${_RectangleCopy}": [
            ["style", "top", '536px'],
            ["style", "height", '3px'],
            ["style", "left", '338px'],
            ["color", "background-color", 'rgba(90,151,186,1.00)']
         ],
         "${_Video1}": [
            ["style", "top", '502px'],
            ["subproperty", "boxShadow.blur", '6px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.37)'],
            ["subproperty", "boxShadow.offsetV", '2px'],
            ["subproperty", "boxShadow.offsetH", '2px'],
            ["style", "left", '0px']
         ],
         "${_EllipseCopy}": [
            ["color", "background-color", 'rgba(178,178,178,1.00)'],
            ["style", "top", '430px'],
            ["style", "left", '525px'],
            ["style", "display", 'block'],
            ["style", "height", '11px'],
            ["style", "opacity", '0.000000'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '12px']
         ],
         "${_Kampanya_ButtonCopy}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "border-bottom-left-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "top", '452px'],
            ["style", "height", '35px'],
            ["style", "left", '306px'],
            ["style", "cursor", 'context-menu'],
            ["style", "width", '136px']
         ],
         "${_seperation_Copy}": [
            ["style", "left", '565px'],
            ["style", "top", '452px']
         ],
         "${__90Gun_text02}": [
            ["style", "top", '144px'],
            ["style", "height", '202px'],
            ["style", "opacity", '0'],
            ["style", "left", '997px'],
            ["style", "width", '482px']
         ],
         "${_Rectangle}": [
            ["color", "background-color", 'rgba(90,151,186,1.00)'],
            ["style", "top", '536px'],
            ["style", "height", '3px']
         ],
         "${__90Gun_text03Copy}": [
            ["style", "top", '144px'],
            ["style", "height", '208px'],
            ["style", "opacity", '1'],
            ["style", "left", '1004px'],
            ["style", "width", '503px']
         ],
         "${_RectangleCopy2}": [
            ["color", "background-color", 'rgba(90,151,186,1.00)'],
            ["style", "top", '101px'],
            ["style", "height", '4px'],
            ["style", "opacity", '1'],
            ["style", "left", '344px'],
            ["style", "width", '496px']
         ],
         "${_Ban_Zemin}": [
            ["style", "left", '0px'],
            ["style", "top", '25px']
         ],
         "${_Kampanya_ButtonCopy3}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "border-bottom-right-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '35px'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '566px'],
            ["style", "width", '113px']
         ],
         "${_icerigi}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '2px'],
            ["style", "font-size", '13px'],
            ["style", "top", '6px'],
            ["style", "width", '124px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Mouse_on1}": [
            ["style", "display", 'none'],
            ["style", "left", '311px'],
            ["style", "top", '470px']
         ],
         "${__90Gun_text04}": [
            ["style", "top", '132px'],
            ["style", "height", '204px'],
            ["style", "opacity", '0'],
            ["style", "left", '983px'],
            ["style", "width", '611px']
         ],
         "${_RectangleCopy3}": [
            ["color", "background-color", 'rgba(90,151,186,1.00)'],
            ["style", "top", '373px'],
            ["style", "height", '4px'],
            ["style", "opacity", '1'],
            ["style", "left", '344px'],
            ["style", "width", '496px']
         ],
         "${_Mouse_on2Copy}": [
            ["style", "display", 'none'],
            ["style", "left", '437px'],
            ["style", "top", '470px']
         ],
         "${_menuG}": [
            ["style", "display", 'none']
         ],
         "${_Text6}": [
            ["style", "top", '52px'],
            ["color", "color", 'rgba(0,174,239,1.00)'],
            ["style", "opacity", '1'],
            ["style", "display", 'none'],
            ["style", "font-size", '29px']
         ],
         "${_seperation_}": [
            ["style", "left", '441px'],
            ["style", "top", '452px']
         ],
         "${_Text}": [
            ["style", "top", '502px'],
            ["style", "width", '196px'],
            ["color", "color", 'rgba(0,25,39,1.00)'],
            ["style", "height", '25px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "left", '669px'],
            ["style", "font-size", '23px']
         ],
         "${_damga2}": [
            ["style", "left", '773px'],
            ["style", "top", '348px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "width", '983px'],
            ["style", "height", '779px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Kampanya_ButtonCopy2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "height", '35px'],
            ["style", "left", '442px'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '124px']
         ],
         "${_Text8}": [
            ["style", "display", 'block'],
            ["style", "opacity", '0']
         ],
         "${_icerigiCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "left", '2px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '124px'],
            ["style", "top", '6px'],
            ["style", "height", '17px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "font-size", '13px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 21500,
         autoPlay: true,
         labels: {
            "Basla": 0,
            "Kampanya": 17596
         },
         timeline: [
            { id: "eid381", tween: [ "style", "${_RectangleCopy2}", "width", '496px', { fromValue: '496px'}], position: 0, duration: 0 },
            { id: "eid572", tween: [ "style", "${_RoundRect2}", "opacity", '1', { fromValue: '0'}], position: 17500, duration: 96 },
            { id: "eid434", tween: [ "style", "${__90Gun_text03Copy}", "width", '503px', { fromValue: '503px'}], position: 7576, duration: 0 },
            { id: "eid479", tween: [ "style", "${__90Gun_text03Copy}", "width", '503px', { fromValue: '503px'}], position: 9561, duration: 0 },
            { id: "eid9", tween: [ "style", "${_Mouse_on1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid389", tween: [ "style", "${_RectangleCopy3}", "top", '373px', { fromValue: '373px'}], position: 0, duration: 0 },
            { id: "eid659", tween: [ "color", "${_Stage}", "background-color", 'rgba(255,255,255,0.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,0.00)'}], position: 0, duration: 0 },
            { id: "eid622", tween: [ "style", "${_Text10}", "display", 'none', { fromValue: 'none'}], position: 20451, duration: 0 },
            { id: "eid625", tween: [ "style", "${_Text10}", "display", 'block', { fromValue: 'none'}], position: 20500, duration: 0 },
            { id: "eid481", tween: [ "style", "${__90Gun_text03Copy}", "left", '348px', { fromValue: '1004px'}], position: 7576, duration: 1985 },
            { id: "eid547", tween: [ "style", "${_icerigiCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid120", tween: [ "style", "${_RoundRect}", "width", '983px', { fromValue: '983px'}], position: 0, duration: 0 },
            { id: "eid159", tween: [ "style", "${_Symbol_12}", "top", '567px', { fromValue: '567px'}], position: 0, duration: 0 },
            { id: "eid569", tween: [ "style", "${_EllipseCopy4}", "opacity", '1', { fromValue: '0.000000'}], position: 17500, duration: 96 },
            { id: "eid414", tween: [ "transform", "${__90Gun_text012}", "scaleX", '1.03', { fromValue: '1'}], position: 1500, duration: 64 },
            { id: "eid416", tween: [ "transform", "${__90Gun_text012}", "scaleX", '1', { fromValue: '1.03'}], position: 1564, duration: 59 },
            { id: "eid521", tween: [ "transform", "${__90Gun_text012}", "scaleX", '0.97', { fromValue: '1'}], position: 1623, duration: 16239 },
            { id: "eid553", tween: [ "style", "${_sloganimiz}", "left", '444px', { fromValue: '444px'}], position: 0, duration: 0 },
            { id: "eid329", tween: [ "style", "${_Text2}", "height", '93px', { fromValue: '93px'}], position: 0, duration: 0 },
            { id: "eid45", tween: [ "style", "${_Text2}", "font-size", '15px', { fromValue: '15px'}], position: 0, duration: 0 },
            { id: "eid313", tween: [ "style", "${_Rectangle2}", "border-bottom-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid217", tween: [ "style", "${_Text5}", "font-size", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid594", tween: [ "style", "${_Text8}", "display", 'none', { fromValue: 'block'}], position: 19296, duration: 0 },
            { id: "eid595", tween: [ "style", "${_Text8}", "display", 'block', { fromValue: 'none'}], position: 19345, duration: 0 },
            { id: "eid331", tween: [ "style", "${_damga2}", "left", '773px', { fromValue: '773px'}], position: 0, duration: 0 },
            { id: "eid649", tween: [ "style", "${_amaciCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid40", tween: [ "style", "${_Text2}", "left", '671px', { fromValue: '671px'}], position: 0, duration: 0 },
            { id: "eid172", tween: [ "style", "${_RoundRect}", "border-top-left-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid435", tween: [ "style", "${__90Gun_text03Copy}", "height", '208px', { fromValue: '208px'}], position: 7576, duration: 0 },
            { id: "eid480", tween: [ "style", "${__90Gun_text03Copy}", "height", '208px', { fromValue: '208px'}], position: 9561, duration: 0 },
            { id: "eid288", tween: [ "color", "${_menu}", "background-color", 'rgba(0,174,239,0.91)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,0.91)'}], position: 0, duration: 0 },
            { id: "eid493", tween: [ "style", "${__90Gun_text04}", "top", '132px', { fromValue: '132px'}], position: 12430, duration: 0 },
            { id: "eid184", tween: [ "subproperty", "${_Video1}", "boxShadow.color", 'rgba(0,0,0,0.37)', { fromValue: 'rgba(0,0,0,0.37)'}], position: 0, duration: 0 },
            { id: "eid316", tween: [ "style", "${_menu}", "height", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "style", "${_Text}", "width", '196px', { fromValue: '196px'}], position: 0, duration: 0 },
            { id: "eid307", tween: [ "gradient", "${_Rectangle2}", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]], { fromValue: [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]}], position: 0, duration: 0 },
            { id: "eid426", tween: [ "style", "${__90Gun_text02}", "left", '348px', { fromValue: '997px'}], position: 3719, duration: 1531 },
            { id: "eid489", tween: [ "style", "${__90Gun_text03Copy}", "opacity", '1', { fromValue: '0'}], position: 7576, duration: 674 },
            { id: "eid491", tween: [ "style", "${__90Gun_text03Copy}", "opacity", '1', { fromValue: '1'}], position: 9561, duration: 0 },
            { id: "eid499", tween: [ "style", "${__90Gun_text03Copy}", "opacity", '0', { fromValue: '1'}], position: 12430, duration: 657 },
            { id: "eid651", tween: [ "style", "${_EllipseCopy4}", "left", '494px', { fromValue: '494px'}], position: 17596, duration: 0 },
            { id: "eid550", tween: [ "color", "${_icerigiCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid63", tween: [ "style", "${_TextCopy}", "height", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid646", tween: [ "style", "${_amaciCopy}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid634", tween: [ "color", "${_Ellipse}", "background-color", 'rgba(104,209,246,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(104,209,246,1.00)'}], position: 17596, duration: 0 },
            { id: "eid636", tween: [ "color", "${_Ellipse}", "background-color", 'rgba(178,178,178,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(104,209,246,1)'}], position: 19296, duration: 49 },
            { id: "eid390", tween: [ "style", "${_RectangleCopy2}", "top", '101px', { fromValue: '101px'}], position: 0, duration: 0 },
            { id: "eid286", tween: [ "style", "${_icerigi}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "color", "${_Rectangle}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid320", tween: [ "style", "${_Rectangle2}", "top", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid47", tween: [ "style", "${_Text}", "font-size", '23px', { fromValue: '23px'}], position: 0, duration: 0 },
            { id: "eid243", tween: [ "style", "${_Text4}", "left", '891px', { fromValue: '891px'}], position: 0, duration: 0 },
            { id: "eid84", tween: [ "style", "${_Text2Copy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid34", tween: [ "style", "${_Text}", "left", '669px', { fromValue: '669px'}], position: 0, duration: 0 },
            { id: "eid330", tween: [ "style", "${_menuG}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid280", tween: [ "style", "${_icerigi}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid315", tween: [ "style", "${_menu}", "border-bottom-left-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid407", tween: [ "style", "${__90Gun_text012}", "width", '486px', { fromValue: '212px'}], position: 250, duration: 1250 },
            { id: "eid186", tween: [ "subproperty", "${_Video1}", "boxShadow.offsetH", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_Mouse_on2Copy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid603", tween: [ "style", "${_Text9}", "opacity", '0', { fromValue: '1'}], position: 20451, duration: 49 },
            { id: "eid604", tween: [ "style", "${_Text9}", "opacity", '1', { fromValue: '0'}], position: 20500, duration: 1000 },
            { id: "eid213", tween: [ "style", "${_Text5}", "top", '711px', { fromValue: '711px'}], position: 0, duration: 0 },
            { id: "eid519", tween: [ "style", "${_RectangleCopy3}", "opacity", '0', { fromValue: '1'}], position: 17500, duration: 96 },
            { id: "eid162", tween: [ "style", "${_Text}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid576", tween: [ "style", "${_icerikText01}", "top", '94px', { fromValue: '94px'}], position: 19296, duration: 0 },
            { id: "eid549", tween: [ "style", "${_icerigiCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid167", tween: [ "style", "${_RoundRect}", "top", '697px', { fromValue: '697px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_RoundRect}", "border-width", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid545", tween: [ "style", "${_icerigiCopy}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid641", tween: [ "style", "${_Text10}", "top", '348px', { fromValue: '348px'}], position: 20500, duration: 0 },
            { id: "eid182", tween: [ "style", "${_Text4}", "top", '635px', { fromValue: '635px'}], position: 0, duration: 0 },
            { id: "eid409", tween: [ "style", "${__90Gun_text012}", "opacity", '1', { fromValue: '0'}], position: 250, duration: 1250 },
            { id: "eid430", tween: [ "style", "${__90Gun_text012}", "opacity", '0', { fromValue: '1'}], position: 3969, duration: 327 },
            { id: "eid509", tween: [ "style", "${_zemin_kose2}", "opacity", '1', { fromValue: '0.450000'}], position: 0, duration: 3000 },
            { id: "eid510", tween: [ "style", "${_zemin_kose2}", "opacity", '0.45', { fromValue: '1'}], position: 3000, duration: 3000 },
            { id: "eid511", tween: [ "style", "${_zemin_kose2}", "opacity", '1', { fromValue: '0.450000'}], position: 6000, duration: 3002 },
            { id: "eid512", tween: [ "style", "${_zemin_kose2}", "opacity", '0.45', { fromValue: '1'}], position: 9002, duration: 3006 },
            { id: "eid513", tween: [ "style", "${_zemin_kose2}", "opacity", '1', { fromValue: '0.450000'}], position: 12008, duration: 3015 },
            { id: "eid514", tween: [ "style", "${_zemin_kose2}", "opacity", '0.45', { fromValue: '1'}], position: 15023, duration: 2477 },
            { id: "eid520", tween: [ "style", "${_zemin_kose2}", "opacity", '0.3', { fromValue: '0.450000'}], position: 17500, duration: 96 },
            { id: "eid427", tween: [ "style", "${__90Gun_text02}", "top", '136px', { fromValue: '144px'}], position: 3719, duration: 1531 },
            { id: "eid582", tween: [ "style", "${_EllipseCopy4}", "display", 'none', { fromValue: 'block'}], position: 17500, duration: 0 },
            { id: "eid587", tween: [ "style", "${_EllipseCopy4}", "display", 'block', { fromValue: 'none'}], position: 17596, duration: 0 },
            { id: "eid62", tween: [ "color", "${_TextCopy}", "color", 'rgba(0,25,39,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,25,39,1.00)'}], position: 0, duration: 0 },
            { id: "eid85", tween: [ "style", "${_RectangleCopy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid79", tween: [ "style", "${_Text2Copy}", "font-size", '15px', { fromValue: '15px'}], position: 0, duration: 0 },
            { id: "eid415", tween: [ "transform", "${__90Gun_text012}", "scaleY", '1.03', { fromValue: '1'}], position: 1500, duration: 64 },
            { id: "eid417", tween: [ "transform", "${__90Gun_text012}", "scaleY", '1', { fromValue: '1.03'}], position: 1564, duration: 59 },
            { id: "eid35", tween: [ "color", "${_Text}", "color", 'rgba(0,25,39,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,25,39,1.00)'}], position: 0, duration: 0 },
            { id: "eid119", tween: [ "style", "${_RoundRect}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid299", tween: [ "transform", "${_Rectangle2}", "rotateZ", '-180deg', { fromValue: '-180deg'}], position: 0, duration: 0 },
            { id: "eid420", tween: [ "style", "${__90Gun_text02}", "width", '482px', { fromValue: '482px'}], position: 3719, duration: 0 },
            { id: "eid438", tween: [ "style", "${__90Gun_text02}", "width", '482px', { fromValue: '482px'}], position: 7308, duration: 0 },
            { id: "eid585", tween: [ "style", "${_RoundRect2}", "display", 'none', { fromValue: 'block'}], position: 17500, duration: 0 },
            { id: "eid590", tween: [ "style", "${_RoundRect2}", "display", 'block', { fromValue: 'none'}], position: 17596, duration: 0 },
            { id: "eid421", tween: [ "style", "${__90Gun_text02}", "height", '202px', { fromValue: '202px'}], position: 3719, duration: 0 },
            { id: "eid439", tween: [ "style", "${__90Gun_text02}", "height", '202px', { fromValue: '202px'}], position: 7308, duration: 0 },
            { id: "eid411", tween: [ "style", "${__90Gun_text012}", "top", '119px', { fromValue: '192px'}], position: 250, duration: 1250 },
            { id: "eid538", tween: [ "style", "${_Text5}", "left", '28px', { fromValue: '28px'}], position: 0, duration: 0 },
            { id: "eid68", tween: [ "color", "${_RectangleCopy}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid428", tween: [ "style", "${__90Gun_text02}", "opacity", '1', { fromValue: '0'}], position: 3719, duration: 1531 },
            { id: "eid445", tween: [ "style", "${__90Gun_text02}", "opacity", '0', { fromValue: '1'}], position: 7308, duration: 692 },
            { id: "eid384", tween: [ "style", "${_RectangleCopy3}", "height", '4px', { fromValue: '4px'}], position: 0, duration: 0 },
            { id: "eid600", tween: [ "style", "${_Text9}", "display", 'none', { fromValue: 'none'}], position: 20451, duration: 0 },
            { id: "eid602", tween: [ "style", "${_Text9}", "display", 'block', { fromValue: 'none'}], position: 20500, duration: 0 },
            { id: "eid104", tween: [ "style", "${_sloganimiz}", "top", '461px', { fromValue: '461px'}], position: 0, duration: 0 },
            { id: "eid311", tween: [ "style", "${_Rectangle2}", "border-top-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid502", tween: [ "style", "${__90Gun_text04}", "opacity", '1', { fromValue: '0'}], position: 12430, duration: 657 },
            { id: "eid515", tween: [ "style", "${__90Gun_text04}", "opacity", '0', { fromValue: '1'}], position: 16606, duration: 711 },
            { id: "eid652", tween: [ "style", "${_Text6}", "display", 'none', { fromValue: 'none'}], position: 17500, duration: 0 },
            { id: "eid654", tween: [ "style", "${_Text6}", "display", 'block', { fromValue: 'none'}], position: 17596, duration: 0 },
            { id: "eid54", tween: [ "style", "${_Text3}", "font-size", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid322", tween: [ "style", "${_icerigi}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid536", tween: [ "style", "${_icerikText01}", "opacity", '1', { fromValue: '0.000000'}], position: 17685, duration: 1611 },
            { id: "eid574", tween: [ "style", "${_icerikText01}", "opacity", '0', { fromValue: '1'}], position: 19296, duration: 1155 },
            { id: "eid392", tween: [ "style", "${_RectangleCopy2}", "left", '344px', { fromValue: '344px'}], position: 0, duration: 0 },
            { id: "eid655", tween: [ "style", "${_Text6}", "opacity", '0', { fromValue: '1'}], position: 17500, duration: 96 },
            { id: "eid657", tween: [ "style", "${_Text6}", "opacity", '1', { fromValue: '0'}], position: 17685, duration: 1611 },
            { id: "eid214", tween: [ "style", "${_Text5}", "height", '35px', { fromValue: '35px'}], position: 0, duration: 0 },
            { id: "eid554", tween: [ "style", "${_sloganimiz}", "width", '121px', { fromValue: '121px'}], position: 0, duration: 0 },
            { id: "eid304", tween: [ "style", "${_Rectangle2}", "height", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid546", tween: [ "style", "${_icerigiCopy}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid157", tween: [ "style", "${_RectangleCopy}", "top", '536px', { fromValue: '536px'}], position: 0, duration: 0 },
            { id: "eid645", tween: [ "style", "${_amaciCopy}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid592", tween: [ "color", "${_icerikText01}", "color", 'rgba(26,77,105,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,25,39,1)'}], position: 17685, duration: 1611 },
            { id: "eid242", tween: [ "style", "${_tamambutton}", "left", '874px', { fromValue: '874px'}], position: 0, duration: 0 },
            { id: "eid638", tween: [ "color", "${_EllipseCopy4}", "background-color", 'rgba(104,209,246,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(178,178,178,1)'}], position: 19296, duration: 49 },
            { id: "eid642", tween: [ "color", "${_EllipseCopy4}", "background-color", 'rgba(178,178,178,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(104,209,246,1)'}], position: 20451, duration: 49 },
            { id: "eid559", tween: [ "style", "${_Text3}", "width", '197px', { fromValue: '197px'}], position: 0, duration: 0 },
            { id: "eid495", tween: [ "style", "${__90Gun_text04}", "height", '204px', { fromValue: '204px'}], position: 12430, duration: 0 },
            { id: "eid117", tween: [ "style", "${_RoundRect}", "height", '56px', { fromValue: '56px'}], position: 0, duration: 0 },
            { id: "eid648", tween: [ "color", "${_amaciCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_Text3}", "height", '11px', { fromValue: '11px'}], position: 0, duration: 0 },
            { id: "eid560", tween: [ "style", "${_Text3}", "left", '786px', { fromValue: '786px'}], position: 0, duration: 0 },
            { id: "eid598", tween: [ "style", "${_Text8}", "opacity", '1', { fromValue: '0'}], position: 19345, duration: 1106 },
            { id: "eid599", tween: [ "style", "${_Text8}", "opacity", '0', { fromValue: '1'}], position: 20451, duration: 1049 },
            { id: "eid46", tween: [ "style", "${_Text}", "height", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid628", tween: [ "style", "${_Text11}", "opacity", '0', { fromValue: '1'}], position: 20451, duration: 49 },
            { id: "eid630", tween: [ "style", "${_Text11}", "opacity", '1', { fromValue: '0'}], position: 20500, duration: 1000 },
            { id: "eid544", tween: [ "style", "${_amaci}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid627", tween: [ "style", "${_Text10}", "opacity", '0', { fromValue: '1'}], position: 20451, duration: 49 },
            { id: "eid629", tween: [ "style", "${_Text10}", "opacity", '1', { fromValue: '0'}], position: 20500, duration: 1000 },
            { id: "eid270", tween: [ "style", "${_menu}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid579", tween: [ "style", "${_EllipseCopy}", "opacity", '1', { fromValue: '0.000000'}], position: 17500, duration: 96 },
            { id: "eid188", tween: [ "subproperty", "${_Video1}", "boxShadow.blur", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid284", tween: [ "style", "${_menu}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid494", tween: [ "style", "${__90Gun_text04}", "width", '611px', { fromValue: '611px'}], position: 12430, duration: 0 },
            { id: "eid264", tween: [ "style", "${_damga2}", "top", '348px', { fromValue: '348px'}], position: 0, duration: 0 },
            { id: "eid555", tween: [ "style", "${_kampanya_hakkinda}", "width", '128px', { fromValue: '128px'}], position: 0, duration: 0 },
            { id: "eid650", tween: [ "style", "${_EllipseCopy}", "left", '525px', { fromValue: '525px'}], position: 17596, duration: 0 },
            { id: "eid187", tween: [ "subproperty", "${_Video1}", "boxShadow.offsetV", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid393", tween: [ "style", "${_RectangleCopy3}", "left", '344px', { fromValue: '344px'}], position: 0, duration: 0 },
            { id: "eid581", tween: [ "style", "${_EllipseCopy}", "display", 'none', { fromValue: 'block'}], position: 17500, duration: 0 },
            { id: "eid586", tween: [ "style", "${_EllipseCopy}", "display", 'block', { fromValue: 'none'}], position: 17596, duration: 0 },
            { id: "eid517", tween: [ "style", "${_RectangleCopy2}", "opacity", '0.01', { fromValue: '1'}], position: 17500, duration: 96 },
            { id: "eid314", tween: [ "style", "${_menu}", "border-bottom-right-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid621", tween: [ "style", "${_Text11}", "display", 'none', { fromValue: 'none'}], position: 20451, duration: 0 },
            { id: "eid626", tween: [ "style", "${_Text11}", "display", 'block', { fromValue: 'none'}], position: 20500, duration: 0 },
            { id: "eid319", tween: [ "style", "${_amaci}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid410", tween: [ "style", "${__90Gun_text012}", "left", '344px', { fromValue: '486px'}], position: 250, duration: 1250 },
            { id: "eid558", tween: [ "style", "${_Text3}", "top", '753px', { fromValue: '753px'}], position: 0, duration: 0 },
            { id: "eid644", tween: [ "color", "${_EllipseCopy}", "background-color", 'rgba(104,209,246,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(178,178,178,1)'}], position: 20451, duration: 49 },
            { id: "eid476", tween: [ "style", "${__90Gun_text03Copy}", "top", '130px', { fromValue: '130px'}], position: 7576, duration: 0 },
            { id: "eid478", tween: [ "style", "${__90Gun_text03Copy}", "top", '130px', { fromValue: '130px'}], position: 9561, duration: 0 },
            { id: "eid382", tween: [ "style", "${_RectangleCopy2}", "height", '4px', { fromValue: '4px'}], position: 0, duration: 0 },
            { id: "eid86", tween: [ "style", "${_TextCopy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid69", tween: [ "style", "${_RectangleCopy}", "height", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid59", tween: [ "color", "${_Text3}", "color", 'rgba(0,174,239,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1.00)'}], position: 0, duration: 0 },
            { id: "eid171", tween: [ "style", "${_RoundRect}", "border-top-right-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid385", tween: [ "color", "${_RectangleCopy3}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid387", tween: [ "style", "${_RectangleCopy3}", "width", '496px', { fromValue: '496px'}], position: 0, duration: 0 },
            { id: "eid160", tween: [ "style", "${_Text2}", "top", '550px', { fromValue: '550px'}], position: 0, duration: 0 },
            { id: "eid66", tween: [ "style", "${_TextCopy}", "width", '196px', { fromValue: '196px'}], position: 0, duration: 0 },
            { id: "eid551", tween: [ "style", "${_kampanya_hakkinda}", "left", '311px', { fromValue: '311px'}], position: 0, duration: 0 },
            { id: "eid408", tween: [ "style", "${__90Gun_text012}", "height", '238px', { fromValue: '124px'}], position: 250, duration: 1250 },
            { id: "eid271", tween: [ "style", "${_menu}", "width", '129px', { fromValue: '129px'}], position: 0, duration: 0 },
            { id: "eid161", tween: [ "style", "${_TextCopy}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid64", tween: [ "style", "${_TextCopy}", "font-size", '23px', { fromValue: '23px'}], position: 0, duration: 0 },
            { id: "eid373", tween: [ "color", "${_RectangleCopy2}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid500", tween: [ "style", "${__90Gun_text04}", "left", '344px', { fromValue: '983px'}], position: 12430, duration: 1735 },
            { id: "eid158", tween: [ "style", "${_Rectangle}", "top", '536px', { fromValue: '536px'}], position: 0, duration: 0 },
            { id: "eid7", tween: [ "style", "${_Mouse_on2Copy2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid367", tween: [ "style", "${_SB-TKHK}", "left", '859px', { fromValue: '859px'}], position: 0, duration: 0 },
            { id: "eid177", tween: [ "style", "${_Text4}", "font-size", '14px', { fromValue: '14px'}], position: 0, duration: 0 },
            { id: "eid291", tween: [ "style", "${_amaci}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid310", tween: [ "style", "${_Rectangle2}", "border-bottom-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid556", tween: [ "style", "${_Text5}", "width", '925px', { fromValue: '925px'}], position: 0, duration: 0 },
            { id: "eid368", tween: [ "style", "${_SB-TKHK}", "top", '38px', { fromValue: '38px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_Rectangle}", "height", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid571", tween: [ "style", "${_Ellipse}", "opacity", '1', { fromValue: '0'}], position: 17500, duration: 96 },
            { id: "eid312", tween: [ "style", "${_Rectangle2}", "border-top-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid584", tween: [ "style", "${_Ellipse}", "display", 'none', { fromValue: 'block'}], position: 17500, duration: 0 },
            { id: "eid589", tween: [ "style", "${_Ellipse}", "display", 'block', { fromValue: 'none'}], position: 17596, duration: 0 },
            { id: "eid176", tween: [ "color", "${_Text4}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1.00)'}], position: 0, duration: 0 },
            { id: "eid620", tween: [ "style", "${_Text9}", "height", '224px', { fromValue: '224px'}], position: 21500, duration: 0 },
            { id: "eid647", tween: [ "style", "${_amaciCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid163", tween: [ "style", "${_Video1}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid165", tween: [ "style", "${_Text2Copy}", "top", '550px', { fromValue: '550px'}], position: 0, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'you_tube_butt',
      type: 'image',
      rect: ['0px','0px','72px','50px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/you_tube_butt.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_you_tube_butt}": [
            ["style", "top", '0px'],
            ["style", "height", '50px'],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["style", "width", '72px']
         ],
         "${symbolSelector}": [
            ["style", "height", '50px'],
            ["style", "width", '72px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: false,
         timeline: [
            { id: "eid17", tween: [ "style", "${_you_tube_butt}", "top", '-7px', { fromValue: '0px'}], position: 0, duration: 500 },
            { id: "eid16", tween: [ "style", "${_you_tube_butt}", "height", '64px', { fromValue: '50px'}], position: 0, duration: 500 },
            { id: "eid19", tween: [ "style", "${_you_tube_butt}", "left", '-10px', { fromValue: '0px'}], position: 0, duration: 500 },
            { id: "eid21", tween: [ "style", "${_you_tube_butt}", "opacity", '0.53', { fromValue: '1'}], position: 0, duration: 500 },
            { id: "eid18", tween: [ "style", "${_you_tube_butt}", "width", '92px', { fromValue: '72px'}], position: 0, duration: 500 }         ]
      }
   }
},
"tamambutton": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['-13px','34px','72px','34px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'RoundRect2',
      stroke: [1,'rgb(0, 0, 0)','solid'],
      type: 'rect',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '33px'],
            ["style", "width", '79px']
         ],
         "${_RoundRect2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "border-color", 'rgba(150,150,150,0.60)'],
            ["style", "height", '27px'],
            ["style", "border-style", 'solid'],
            ["style", "border-width", '3px'],
            ["style", "width", '73px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 250,
         autoPlay: false,
         timeline: [
            { id: "eid98", tween: [ "style", "${_RoundRect2}", "height", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid175", tween: [ "style", "${_RoundRect2}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid99", tween: [ "style", "${_RoundRect2}", "width", '73px', { fromValue: '73px'}], position: 0, duration: 0 },
            { id: "eid100", tween: [ "style", "${_RoundRect2}", "border-width", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid173", tween: [ "color", "${_RoundRect2}", "border-color", 'rgba(150,150,150,0.60)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(150,150,150,0.60)'}], position: 0, duration: 0 },
            { id: "eid189", tween: [ "color", "${_RoundRect2}", "background-color", 'rgba(0,104,143,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1)'}], position: 0, duration: 250 },
            { id: "eid174", tween: [ "style", "${_RoundRect2}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 }         ]
      }
   }
},
"ic_button": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'sol_ic',
      type: 'image',
      rect: ['-3px','-10px','40px','40px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/sol_ic.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '40px'],
            ["style", "width", '40px']
         ],
         "${_sol_ic}": [
            ["style", "top", '0px'],
            ["transform", "scaleY", '1'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '0.60037267208099'],
            ["style", "left", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 192.03515625,
         autoPlay: false,
         timeline: [
            { id: "eid352", tween: [ "style", "${_sol_ic}", "opacity", '0.38604798913002', { fromValue: '0.6003730297088623'}], position: 0, duration: 192 },
            { id: "eid341", tween: [ "style", "${_sol_ic}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid349", tween: [ "transform", "${_sol_ic}", "scaleY", '1.21813', { fromValue: '1'}], position: 0, duration: 192 },
            { id: "eid340", tween: [ "style", "${_sol_ic}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid348", tween: [ "transform", "${_sol_ic}", "scaleX", '1.21813', { fromValue: '1'}], position: 0, duration: 192 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-91745653");
