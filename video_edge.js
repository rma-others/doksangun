/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Ban_Zemin',
            type:'image',
            rect:['0px','25px','983px','427px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Ban_Zemin.jpg",'0px','0px']
         },
         {
            id:'Kampanya_ButtonCopy',
            type:'rect',
            rect:['306px','452px','136px','35px','auto','auto'],
            cursor:['context-menu'],
            borderRadius:["0px","0px","0px","9px 9px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy2',
            type:'rect',
            rect:['442px','452px','124px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","0px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Kampanya_ButtonCopy3',
            type:'rect',
            rect:['566px','452px','113px','35px','auto','auto'],
            cursor:['pointer'],
            borderRadius:["0px","0px","9px 9px","0px"],
            fill:["rgba(0,174,239,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'seperation_',
            type:'image',
            rect:['441px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'seperation_Copy',
            type:'image',
            rect:['565px','452px','2px','34px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"seperation_.png",'0px','0px']
         },
         {
            id:'Mouse_on1',
            display:'none',
            type:'image',
            rect:['311px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy',
            display:'none',
            type:'image',
            rect:['437px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'Mouse_on2Copy2',
            display:'none',
            type:'image',
            rect:['550px','470px','134px','18px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Mouse_on.png",'0px','0px']
         },
         {
            id:'kampanya_hakkinda',
            type:'text',
            rect:['306px','462px','140px','17px','auto','auto'],
            cursor:['context-menu'],
            text:"Çalışma hakkında",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimiz',
            type:'text',
            rect:['448px','462px','106px','17px','auto','auto'],
            cursor:['pointer'],
            text:"90 Gün Teması",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'sloganimizCopy',
            type:'text',
            rect:['567px','462px','112px','17px','auto','auto'],
            cursor:['pointer'],
            text:"Bize ulaşın",
            align:"center",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","600","none",""]
         },
         {
            id:'Video1',
            type:'image',
            rect:['0px','501px','318px','180px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Video1.jpg",'0px','0px'],
            boxShadow:["",3,3,3,0,"rgba(0,0,0,0.65)"]
         },
         {
            id:'Symbol_12',
            type:'rect',
            rect:['117','559','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'Rectangle',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'RectangleCopy',
            type:'rect',
            rect:['671px','616px','312px','17px','auto','auto'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'Text',
            type:'text',
            rect:['697px','412px','10px','17px','auto','auto'],
            text:"Beni Haberdar Et",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',24,"rgba(0,0,0,1)","normal","none",""]
         },
         {
            id:'TextCopy',
            type:'text',
            rect:['697px','412px','10px','17px','auto','auto'],
            text:"Günün Mesajı",
            font:['Trebuchet MS, Arial, Helvetica, sans-serif',24,"rgba(0,0,0,1)","normal","none",""]
         },
         {
            id:'Text2',
            type:'text',
            rect:['671px','634px','312px','84px','auto','auto'],
            text:"90 Gün Çalışması ve Türkiye Kamu Hastaneleri'ne ilişkin bilgi almak için\"Tamam\" butonunu tıklayarak e-bülten'e üye olabilirsiniz.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',24,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'Text2Copy',
            type:'text',
            rect:['671px','634px','312px','84px','auto','auto'],
            text:"Kararlı bir yaklaşımla ve hizmete inanmış kişilerle, 90 günde bir gökdelen inşa etmek mümkündür.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',24,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'Text3',
            type:'text',
            rect:['877','768','auto','auto','auto','auto'],
            text:"©Copyright 2013 Türkiye Kamu Hastaneleri Kurumu",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',15,"rgba(0,25,39,1)","500","none","normal"]
         },
         {
            id:'tamambutton',
            type:'rect',
            rect:['887','626','auto','auto','auto','auto'],
            cursor:['pointer']
         },
         {
            id:'Text4',
            type:'text',
            rect:['898','643','auto','auto','auto','auto'],
            cursor:['pointer'],
            text:"Tamam",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',8,"rgba(0,174,239,1)","500","none","normal"]
         },
         {
            id:'RoundRect',
            type:'rect',
            rect:['1px','701px','977px','56px','auto','auto'],
            fill:["rgba(0,174,239,1)"],
            stroke:[1,"rgb(150, 150, 150)","none"]
         },
         {
            id:'Text5',
            type:'text',
            rect:['57','710','auto','auto','auto','auto'],
            cursor:['wait'],
            text:"90 Gün Çalışması'nın tamamlanmasına 80 Gün kaldı.",
            align:"center",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',14,"rgba(255,255,255,1)","500","none","normal"]
         },
         {
            id:'menuG',
            display:'none',
            type:'group',
            rect:['318','487','129','49','auto','auto'],
            c:[
            {
               id:'menu',
               type:'rect',
               rect:['6px','0px','124px','50px','auto','auto'],
               fill:["rgba(192,192,192,1)"],
               stroke:[0,"rgba(0,0,0,1)","none"]
            },
            {
               id:'Rectangle2',
               type:'rect',
               rect:['0px','23px','123px','3px','auto','auto'],
               fill:["rgba(0,174,239,0.9063)"],
               stroke:[0,"rgb(0, 0, 0)","none"]
            },
            {
               id:'amaci',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'amaciCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma amacı",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigi',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            },
            {
               id:'icerigiCopy',
               display:'none',
               type:'text',
               rect:['-11px','-25px','140px','17px','auto','auto'],
               cursor:['pointer'],
               text:"Çalışma içeriği",
               align:"center",
               font:['Trebuchet MS, Arial, Helvetica, sans-serif',13,"rgba(255,255,255,1.00)","400","none",""]
            }]
         },
         {
            id:'Turuncu_bant',
            type:'image',
            rect:['0px','0px','983px','25px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Turuncu_bant.png",'0px','0px'],
            boxShadow:["",0,3,4,-2,"rgba(0,0,0,0.65)"]
         },
         {
            id:'myVideo',
            type:'rect',
            rect:['144px','35px','720px','409px','auto','auto'],
            fill:["rgba(218,218,218,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"]
         }],
         symbolInstances: [
         {
            id:'Symbol_12',
            symbolName:'Symbol_1'
         },
         {
            id:'tamambutton',
            symbolName:'tamambutton'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Rectangle2}": [
            ["style", "top", '25px'],
            ["style", "border-top-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["transform", "rotateZ", '-180deg'],
            ["style", "border-bottom-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '1px'],
            ["gradient", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]],
            ["style", "border-top-right-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-left-radius", [5,5], {valueTemplate:'@@0@@px @@1@@px'} ]
         ],
         "${_sloganimizCopy}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '567px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '112px']
         ],
         "${_sloganimiz}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '444px'],
            ["style", "font-size", '13px'],
            ["style", "top", '461px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '121px']
         ],
         "${_Text2}": [
            ["style", "top", '550px'],
            ["style", "height", '93px'],
            ["style", "font-weight", '500'],
            ["style", "left", '671px'],
            ["style", "font-size", '15px']
         ],
         "${_kampanya_hakkinda}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '600'],
            ["style", "left", '311px'],
            ["style", "font-size", '13px'],
            ["style", "top", '462px'],
            ["style", "cursor", 'context-menu'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '128px']
         ],
         "${_Text4}": [
            ["style", "top", '635px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '891px'],
            ["style", "font-size", '14px']
         ],
         "${_amaciCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "width", '124px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1px'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "height", '17px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_menu}": [
            ["style", "top", '0px'],
            ["style", "border-bottom-left-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "border-bottom-right-radius", [8,8], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '52px'],
            ["color", "background-color", 'rgba(0,174,239,0.91)'],
            ["style", "left", '0px'],
            ["style", "width", '129px']
         ],
         "${_TextCopy}": [
            ["style", "top", '502px'],
            ["style", "font-size", '23px'],
            ["style", "height", '25px'],
            ["color", "color", 'rgba(0,25,39,1.00)'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "left", '338px'],
            ["style", "width", '196px']
         ],
         "${_Symbol_12}": [
            ["style", "top", '567px'],
            ["style", "left", '123px'],
            ["style", "cursor", 'pointer']
         ],
         "${_myVideo}": [
            ["style", "height", '409px'],
            ["style", "top", '35px'],
            ["style", "left", '144px'],
            ["style", "width", '720px']
         ],
         "${_RoundRect}": [
            ["style", "top", '697px'],
            ["style", "border-top-left-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "left", '0px'],
            ["style", "border-top-right-radius", [14,14], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '56px'],
            ["style", "border-style", 'none'],
            ["style", "border-width", '1px'],
            ["style", "width", '983px']
         ],
         "${_tamambutton}": [
            ["style", "left", '874px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Text3}": [
            ["style", "top", '753px'],
            ["style", "width", '197px'],
            ["style", "cursor", 'auto'],
            ["color", "color", 'rgba(0,174,239,1.00)'],
            ["style", "height", '11px'],
            ["style", "left", '786px'],
            ["style", "font-size", '8px']
         ],
         "${_Turuncu_bant}": [
            ["style", "top", '0px'],
            ["subproperty", "boxShadow.blur", '4px'],
            ["subproperty", "boxShadow.spread", '-2px'],
            ["style", "left", '0px'],
            ["subproperty", "boxShadow.offsetV", '3px'],
            ["subproperty", "boxShadow.offsetH", '0px'],
            ["subproperty", "boxShadow.color", 'rgba(41,41,41,0.65)']
         ],
         "${_Text5}": [
            ["style", "top", '711px'],
            ["style", "text-align", 'center'],
            ["style", "font-size", '27px'],
            ["style", "height", '35px'],
            ["style", "cursor", 'wait'],
            ["style", "left", '28px'],
            ["style", "width", '925px']
         ],
         "${_RectangleCopy}": [
            ["color", "background-color", 'rgba(90,151,186,1.00)'],
            ["style", "top", '536px'],
            ["style", "left", '338px'],
            ["style", "height", '3px']
         ],
         "${_Video1}": [
            ["style", "top", '502px'],
            ["subproperty", "boxShadow.blur", '6px'],
            ["subproperty", "boxShadow.color", 'rgba(0,0,0,0.37)'],
            ["subproperty", "boxShadow.offsetV", '2px'],
            ["subproperty", "boxShadow.offsetH", '2px'],
            ["style", "left", '0px']
         ],
         "${_Mouse_on2Copy}": [
            ["style", "display", 'none'],
            ["style", "left", '437px'],
            ["style", "top", '470px']
         ],
         "${_Rectangle}": [
            ["style", "height", '3px'],
            ["color", "background-color", 'rgba(90,151,186,1.00)'],
            ["style", "top", '536px']
         ],
         "${_Ban_Zemin}": [
            ["style", "left", '0px'],
            ["style", "top", '25px']
         ],
         "${_icerigi}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '2px'],
            ["style", "font-size", '13px'],
            ["style", "top", '6px'],
            ["style", "width", '124px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Kampanya_ButtonCopy}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "border-bottom-left-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "top", '452px'],
            ["style", "height", '35px'],
            ["style", "left", '306px'],
            ["style", "cursor", 'context-menu'],
            ["style", "width", '136px']
         ],
         "${_amaci}": [
            ["style", "letter-spacing", '0px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "left", '1px'],
            ["style", "font-size", '13px'],
            ["style", "top", '29px'],
            ["style", "cursor", 'pointer'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "height", '17px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "width", '124px']
         ],
         "${_Mouse_on2Copy2}": [
            ["style", "top", '470px'],
            ["style", "left", '550px'],
            ["style", "display", 'none']
         ],
         "${_Kampanya_ButtonCopy3}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "border-bottom-right-radius", [9,9], {valueTemplate:'@@0@@px @@1@@px'} ],
            ["style", "height", '35px'],
            ["style", "cursor", 'pointer'],
            ["style", "left", '566px'],
            ["style", "width", '113px']
         ],
         "${_menuG}": [
            ["style", "display", 'none']
         ],
         "${_Text}": [
            ["style", "top", '502px'],
            ["style", "width", '196px'],
            ["color", "color", 'rgba(0,25,39,1.00)'],
            ["style", "height", '25px'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "left", '669px'],
            ["style", "font-size", '23px']
         ],
         "${_seperation_}": [
            ["style", "left", '441px'],
            ["style", "top", '452px']
         ],
         "${_Mouse_on1}": [
            ["style", "display", 'none'],
            ["style", "left", '311px'],
            ["style", "top", '470px']
         ],
         "${_Text2Copy}": [
            ["style", "top", '550px'],
            ["style", "font-weight", '500'],
            ["style", "left", '338px'],
            ["style", "font-size", '15px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "width", '983px'],
            ["style", "height", '779px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Kampanya_ButtonCopy2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '452px'],
            ["style", "height", '35px'],
            ["style", "left", '442px'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '124px']
         ],
         "${_seperation_Copy}": [
            ["style", "left", '565px'],
            ["style", "top", '452px']
         ],
         "${_icerigiCopy}": [
            ["style", "letter-spacing", '0px'],
            ["style", "left", '2px'],
            ["color", "color", 'rgba(255,141,0,1.00)'],
            ["style", "font-weight", '400'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '124px'],
            ["style", "top", '6px'],
            ["style", "height", '17px'],
            ["style", "text-align", 'center'],
            ["style", "text-indent", '0px'],
            ["style", "display", 'none'],
            ["style", "font-family", 'Trebuchet MS, Arial, Helvetica, sans-serif'],
            ["style", "word-spacing", '0px'],
            ["style", "font-size", '13px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         labels: {
            "Basla": 0
         },
         timeline: [
            { id: "eid177", tween: [ "style", "${_Text4}", "font-size", '14px', { fromValue: '14px'}], position: 0, duration: 0 },
            { id: "eid62", tween: [ "color", "${_TextCopy}", "color", 'rgba(0,25,39,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,25,39,1.00)'}], position: 0, duration: 0 },
            { id: "eid291", tween: [ "style", "${_amaci}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid85", tween: [ "style", "${_RectangleCopy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid9", tween: [ "style", "${_Mouse_on1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid79", tween: [ "style", "${_Text2Copy}", "font-size", '15px', { fromValue: '15px'}], position: 0, duration: 0 },
            { id: "eid659", tween: [ "color", "${_Stage}", "background-color", 'rgba(255,255,255,0.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,0.00)'}], position: 0, duration: 0 },
            { id: "eid35", tween: [ "color", "${_Text}", "color", 'rgba(0,25,39,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,25,39,1.00)'}], position: 0, duration: 0 },
            { id: "eid120", tween: [ "style", "${_RoundRect}", "width", '983px', { fromValue: '983px'}], position: 0, duration: 0 },
            { id: "eid86", tween: [ "style", "${_TextCopy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid159", tween: [ "style", "${_Symbol_12}", "top", '567px', { fromValue: '567px'}], position: 0, duration: 0 },
            { id: "eid553", tween: [ "style", "${_sloganimiz}", "left", '444px', { fromValue: '444px'}], position: 0, duration: 0 },
            { id: "eid329", tween: [ "style", "${_Text2}", "height", '93px', { fromValue: '93px'}], position: 0, duration: 0 },
            { id: "eid45", tween: [ "style", "${_Text2}", "font-size", '15px', { fromValue: '15px'}], position: 0, duration: 0 },
            { id: "eid313", tween: [ "style", "${_Rectangle2}", "border-bottom-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid68", tween: [ "color", "${_RectangleCopy}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid187", tween: [ "subproperty", "${_Video1}", "boxShadow.offsetV", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid322", tween: [ "style", "${_icerigi}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid649", tween: [ "style", "${_amaciCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid311", tween: [ "style", "${_Rectangle2}", "border-top-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid40", tween: [ "style", "${_Text2}", "left", '671px', { fromValue: '671px'}], position: 0, duration: 0 },
            { id: "eid172", tween: [ "style", "${_RoundRect}", "border-top-left-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid54", tween: [ "style", "${_Text3}", "font-size", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid288", tween: [ "color", "${_menu}", "background-color", 'rgba(0,174,239,0.91)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,0.91)'}], position: 0, duration: 0 },
            { id: "eid214", tween: [ "style", "${_Text5}", "height", '35px', { fromValue: '35px'}], position: 0, duration: 0 },
            { id: "eid554", tween: [ "style", "${_sloganimiz}", "width", '121px', { fromValue: '121px'}], position: 0, duration: 0 },
            { id: "eid546", tween: [ "style", "${_icerigiCopy}", "left", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid648", tween: [ "color", "${_amaciCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid184", tween: [ "subproperty", "${_Video1}", "boxShadow.color", 'rgba(0,0,0,0.37)', { fromValue: 'rgba(0,0,0,0.37)'}], position: 0, duration: 0 },
            { id: "eid645", tween: [ "style", "${_amaciCopy}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid117", tween: [ "style", "${_RoundRect}", "height", '56px', { fromValue: '56px'}], position: 0, duration: 0 },
            { id: "eid316", tween: [ "style", "${_menu}", "height", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "style", "${_Text}", "width", '196px', { fromValue: '196px'}], position: 0, duration: 0 },
            { id: "eid310", tween: [ "style", "${_Rectangle2}", "border-bottom-left-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid304", tween: [ "style", "${_Rectangle2}", "height", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid284", tween: [ "style", "${_menu}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid242", tween: [ "style", "${_tamambutton}", "left", '874px', { fromValue: '874px'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_Text3}", "height", '11px', { fromValue: '11px'}], position: 0, duration: 0 },
            { id: "eid312", tween: [ "style", "${_Rectangle2}", "border-top-right-radius", [5,5], { valueTemplate: '@@0@@px @@1@@px', fromValue: [5,5]}], position: 0, duration: 0 },
            { id: "eid104", tween: [ "style", "${_sloganimiz}", "top", '461px', { fromValue: '461px'}], position: 0, duration: 0 },
            { id: "eid46", tween: [ "style", "${_Text}", "height", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid550", tween: [ "color", "${_icerigiCopy}", "color", 'rgba(255,141,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,141,0,1.00)'}], position: 0, duration: 0 },
            { id: "eid544", tween: [ "style", "${_amaci}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid63", tween: [ "style", "${_TextCopy}", "height", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid270", tween: [ "style", "${_menu}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid538", tween: [ "style", "${_Text5}", "left", '28px', { fromValue: '28px'}], position: 0, duration: 0 },
            { id: "eid188", tween: [ "subproperty", "${_Video1}", "boxShadow.blur", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid560", tween: [ "style", "${_Text3}", "left", '786px', { fromValue: '786px'}], position: 0, duration: 0 },
            { id: "eid119", tween: [ "style", "${_RoundRect}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid176", tween: [ "color", "${_Text4}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1.00)'}], position: 0, duration: 0 },
            { id: "eid555", tween: [ "style", "${_kampanya_hakkinda}", "width", '128px', { fromValue: '128px'}], position: 0, duration: 0 },
            { id: "eid307", tween: [ "gradient", "${_Rectangle2}", "background-image", [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]], { fromValue: [356,[['rgba(0,174,239,1.00)',0],['rgba(255,255,255,0.53)',100]]]}], position: 0, duration: 0 },
            { id: "eid286", tween: [ "style", "${_icerigi}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid330", tween: [ "style", "${_menuG}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "color", "${_Rectangle}", "background-color", 'rgba(90,151,186,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(90,151,186,1.00)'}], position: 0, duration: 0 },
            { id: "eid320", tween: [ "style", "${_Rectangle2}", "top", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid314", tween: [ "style", "${_menu}", "border-bottom-right-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid47", tween: [ "style", "${_Text}", "font-size", '23px', { fromValue: '23px'}], position: 0, duration: 0 },
            { id: "eid243", tween: [ "style", "${_Text4}", "left", '891px', { fromValue: '891px'}], position: 0, duration: 0 },
            { id: "eid59", tween: [ "color", "${_Text3}", "color", 'rgba(0,174,239,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1.00)'}], position: 0, duration: 0 },
            { id: "eid84", tween: [ "style", "${_Text2Copy}", "left", '338px', { fromValue: '338px'}], position: 0, duration: 0 },
            { id: "eid34", tween: [ "style", "${_Text}", "left", '669px', { fromValue: '669px'}], position: 0, duration: 0 },
            { id: "eid171", tween: [ "style", "${_RoundRect}", "border-top-right-radius", [14,14], { valueTemplate: '@@0@@px @@1@@px', fromValue: [14,14]}], position: 0, duration: 0 },
            { id: "eid66", tween: [ "style", "${_TextCopy}", "width", '196px', { fromValue: '196px'}], position: 0, duration: 0 },
            { id: "eid167", tween: [ "style", "${_RoundRect}", "top", '697px', { fromValue: '697px'}], position: 0, duration: 0 },
            { id: "eid69", tween: [ "style", "${_RectangleCopy}", "height", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid280", tween: [ "style", "${_icerigi}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid315", tween: [ "style", "${_menu}", "border-bottom-left-radius", [8,8], { valueTemplate: '@@0@@px @@1@@px', fromValue: [8,8]}], position: 0, duration: 0 },
            { id: "eid551", tween: [ "style", "${_kampanya_hakkinda}", "left", '311px', { fromValue: '311px'}], position: 0, duration: 0 },
            { id: "eid64", tween: [ "style", "${_TextCopy}", "font-size", '23px', { fromValue: '23px'}], position: 0, duration: 0 },
            { id: "eid160", tween: [ "style", "${_Text2}", "top", '550px', { fromValue: '550px'}], position: 0, duration: 0 },
            { id: "eid271", tween: [ "style", "${_menu}", "width", '129px', { fromValue: '129px'}], position: 0, duration: 0 },
            { id: "eid186", tween: [ "subproperty", "${_Video1}", "boxShadow.offsetH", '2px', { fromValue: '2px'}], position: 0, duration: 0 },
            { id: "eid161", tween: [ "style", "${_TextCopy}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_Mouse_on2Copy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid162", tween: [ "style", "${_Text}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid213", tween: [ "style", "${_Text5}", "top", '711px', { fromValue: '711px'}], position: 0, duration: 0 },
            { id: "eid163", tween: [ "style", "${_Video1}", "top", '502px', { fromValue: '502px'}], position: 0, duration: 0 },
            { id: "eid165", tween: [ "style", "${_Text2Copy}", "top", '550px', { fromValue: '550px'}], position: 0, duration: 0 },
            { id: "eid158", tween: [ "style", "${_Rectangle}", "top", '536px', { fromValue: '536px'}], position: 0, duration: 0 },
            { id: "eid319", tween: [ "style", "${_amaci}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid549", tween: [ "style", "${_icerigiCopy}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid7", tween: [ "style", "${_Mouse_on2Copy2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid556", tween: [ "style", "${_Text5}", "width", '925px', { fromValue: '925px'}], position: 0, duration: 0 },
            { id: "eid647", tween: [ "style", "${_amaciCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_RoundRect}", "border-width", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid545", tween: [ "style", "${_icerigiCopy}", "top", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_Rectangle}", "height", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid559", tween: [ "style", "${_Text3}", "width", '197px', { fromValue: '197px'}], position: 0, duration: 0 },
            { id: "eid558", tween: [ "style", "${_Text3}", "top", '753px', { fromValue: '753px'}], position: 0, duration: 0 },
            { id: "eid182", tween: [ "style", "${_Text4}", "top", '635px', { fromValue: '635px'}], position: 0, duration: 0 },
            { id: "eid547", tween: [ "style", "${_icerigiCopy}", "width", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid299", tween: [ "transform", "${_Rectangle2}", "rotateZ", '-180deg', { fromValue: '-180deg'}], position: 0, duration: 0 },
            { id: "eid217", tween: [ "style", "${_Text5}", "font-size", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid646", tween: [ "style", "${_amaciCopy}", "left", '1px', { fromValue: '1px'}], position: 0, duration: 0 },
            { id: "eid157", tween: [ "style", "${_RectangleCopy}", "top", '536px', { fromValue: '536px'}], position: 0, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'you_tube_butt',
      type: 'image',
      rect: ['0px','0px','72px','50px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/you_tube_butt.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_you_tube_butt}": [
            ["style", "top", '0px'],
            ["style", "height", '50px'],
            ["style", "opacity", '1'],
            ["style", "left", '0px'],
            ["style", "width", '72px']
         ],
         "${symbolSelector}": [
            ["style", "height", '50px'],
            ["style", "width", '72px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: false,
         timeline: [
            { id: "eid17", tween: [ "style", "${_you_tube_butt}", "top", '-7px', { fromValue: '0px'}], position: 0, duration: 500 },
            { id: "eid16", tween: [ "style", "${_you_tube_butt}", "height", '64px', { fromValue: '50px'}], position: 0, duration: 500 },
            { id: "eid19", tween: [ "style", "${_you_tube_butt}", "left", '-10px', { fromValue: '0px'}], position: 0, duration: 500 },
            { id: "eid21", tween: [ "style", "${_you_tube_butt}", "opacity", '0.53', { fromValue: '1'}], position: 0, duration: 500 },
            { id: "eid18", tween: [ "style", "${_you_tube_butt}", "width", '92px', { fromValue: '72px'}], position: 0, duration: 500 }         ]
      }
   }
},
"tamambutton": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['-13px','34px','72px','34px','auto','auto'],
      borderRadius: ['10px','10px','10px','10px'],
      id: 'RoundRect2',
      stroke: [1,'rgb(0, 0, 0)','solid'],
      type: 'rect',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '33px'],
            ["style", "width", '79px']
         ],
         "${_RoundRect2}": [
            ["color", "background-color", 'rgba(0,174,239,1.00)'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["color", "border-color", 'rgba(150,150,150,0.60)'],
            ["style", "height", '27px'],
            ["style", "border-style", 'solid'],
            ["style", "border-width", '3px'],
            ["style", "width", '73px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 250,
         autoPlay: false,
         timeline: [
            { id: "eid98", tween: [ "style", "${_RoundRect2}", "height", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid175", tween: [ "style", "${_RoundRect2}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid99", tween: [ "style", "${_RoundRect2}", "width", '73px', { fromValue: '73px'}], position: 0, duration: 0 },
            { id: "eid100", tween: [ "style", "${_RoundRect2}", "border-width", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid173", tween: [ "color", "${_RoundRect2}", "border-color", 'rgba(150,150,150,0.60)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(150,150,150,0.60)'}], position: 0, duration: 0 },
            { id: "eid189", tween: [ "color", "${_RoundRect2}", "background-color", 'rgba(0,104,143,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(0,174,239,1)'}], position: 0, duration: 250 },
            { id: "eid174", tween: [ "style", "${_RoundRect2}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 }         ]
      }
   }
},
"ic_button": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'sol_ic',
      type: 'image',
      rect: ['-3px','-10px','40px','40px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/sol_ic.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '40px'],
            ["style", "width", '40px']
         ],
         "${_sol_ic}": [
            ["style", "top", '0px'],
            ["transform", "scaleY", '1'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '0.60037267208099'],
            ["style", "left", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 192.03515625,
         autoPlay: false,
         timeline: [
            { id: "eid352", tween: [ "style", "${_sol_ic}", "opacity", '0.38604798913002', { fromValue: '0.6003730297088623'}], position: 0, duration: 192 },
            { id: "eid341", tween: [ "style", "${_sol_ic}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid349", tween: [ "transform", "${_sol_ic}", "scaleY", '1.21813', { fromValue: '1'}], position: 0, duration: 192 },
            { id: "eid340", tween: [ "style", "${_sol_ic}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid348", tween: [ "transform", "${_sol_ic}", "scaleX", '1.21813', { fromValue: '1'}], position: 0, duration: 192 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-91745653");
